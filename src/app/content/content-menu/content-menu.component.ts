import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuService } from '../../services/menu.service';
import { MenuElement } from './menu-element';

interface LooseObject {
    [key: string]: any;
}

@Component({
    selector: 'app-content-menu',
    templateUrl: './content-menu.component.html',
    styleUrls: ['./content-menu.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ContentMenuComponent implements OnInit {
    public menuData: LooseObject;

    constructor(/*īīīīīīīīīī|=+=|īīīīīīīīīīī*/
                private menuService: MenuService,
                /*īīīīīīīīīī|=+=|īīīīīīīīīīī*/) {
    }

    ngOnInit() {
        // console.log(
        //     '\n::::::::::::::::::::::::::::::::::::::  ContentMenuComponent  ngOnInit  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.getData();
    }

    public getData(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: AppComponent dataUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.menuService.getDatas().subscribe(
            (data) => {
                this.menuData = data;
                // console.log(
                //     '\n:::::::::::::::::::::::::::::::::::::: AppComponent getData :::::::::::::::::::::::::::::::::::::::::::::::::::',
                //     '\n::this::', this,
                //     '\n::data::', data,
                //     '\n::this.menuData::', this.menuData,
                //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                // );
            },
            (error) => {
                console.error(
                    '\n::::::::::::::::::::::::::::::::::::::  AppComponent  GET  Error  ::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::POST Request Error ::', error,
                    '\n::POST Request Error Message ::', error.message,
                    '\n::POST Request Error Message ::', error.error.message,
                    '\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            () => this.dataUpdate(this.menuData)
        );
    }

    private dataUpdate(data): void {
        // const localData: any = data;
        /*console.log(
            '\n:::::::::::::::::::::::::::::::::::::: AppComponent dataUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::data::', data,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/
    }
}
