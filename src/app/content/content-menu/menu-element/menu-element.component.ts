import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MenuElement } from '../menu-element';

@Component({
    selector: 'app-menu-element',
    templateUrl: './menu-element.component.html',
    styleUrls: ['./menu-element.component.scss']
})
export class MenuElementComponent implements OnInit {
    @Input() items: MenuElement[];
    @ViewChild('childMenu', {static: true}) childMenu: ElementRef;

    // @ViewChild('childMenu', {static: false}) public childMenu;

    constructor(/*[][][][:::::::|+|:::::::][][][]*/
                public router: Router
                /*[][][][:::::::|+|:::::::][][][]*/) {
        // console.log(
        //     '\n::::::::::::::::::::::::::::::::::::::  MenuElementComponent  constructor  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.childMenu::', this.childMenu,
        //     '\n::this.items::', this.items,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    ngOnInit() {
        // console.log(
        //     '\n::::::::::::::::::::::::::::::::::::::  MenuElementComponent  ngOnInit  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.items::', this.items,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

}
