export interface MenuElement {
    displayName: string;
    disabled?: boolean;
    iconName: string;
    route?: string;
    children?: MenuElement[];
}
