import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';

import { ContentRoutingModule } from './content-routing.module';

import { ContentComponent } from './content.component';
import { ForceDirectedGraphComponent } from './d3/views/flows/network/force-directed-graph/force-directed-graph.component';
import { ContentMenuComponent } from './content-menu/content-menu.component';
import { MenuElementComponent } from './content-menu/menu-element/menu-element.component';
import { UsZoomableComponent } from './d3/views/maps/geo/us-zoomable/us-zoomable.component';
import { GraphLinkComponent } from './d3/views/shared/graph-link/graph-link.component';
import { GraphNodeComponent } from './d3/views/shared/graph-node/graph-node.component';
import { GraphComponent } from './d3/views/flows/network/force-directed-graph/graph/graph.component';
import { MapUsComponent } from './d3/views/maps/geo/us-zoomable/map-us/map-us.component';
import { RectangleComponent } from './d3/views/shared/rectangle/rectangle.component';
import { GeoPathComponent } from './d3/views/shared/geo-path/geo-path.component';
import { UsaStaticComponent } from './d3/views/maps/geo/usa-static/usa-static.component';
import { ForceDirectedTreeComponent } from './d3/views/flows/network/force-directed-tree/force-directed-tree.component';
import { HierarchyTreeComponent } from './d3/views/flows/network/force-directed-tree/hierarchy-tree/hierarchy-tree.component';
import { TreeNodeComponent } from './d3/views/shared/tree-node/tree-node.component';
import { ForceDirectedGraphExternalComponent } from './d3/views/flows/network/force-directed-graph-external/force-directed-graph-external.component';
import { TreeGraphComponent } from './d3/views/flows/network/force-directed-graph-external/tree-graph/tree-graph.component';
import { GraphLinkPathComponent } from './d3/views/shared/graph-link-path/graph-link-path.component';
import { NodeContentComponent } from './d3/views/shared/node-content/node-content.component';
import { DynamicComponentLoaderComponent } from './d3/views/shared/dynamic-component-loader/dynamic-component-loader.component';

import { DraggableDirective } from '../directives/draggable.directive';
import { ZoomableDirective } from '../directives/zoomable.directive';
import { ZoomableToBoundingDirective } from '../directives/zoomable-to-bounding.directive';
import { BoundingZoomResetDirective } from '../directives/bounding-zoom-reset.directive';
import { ZoomByPercentageDirective } from '../directives/zoom-by-percentage.directive';
import { DraggableTreeDirective } from '../directives/draggable-tree.directive';
import { ContentHostDirective } from '../directives/content-host.directive';

import { SystemMessageComponent } from './d3/views/shared/system-message/system-message.component';
import { BarChartVerticalComponent } from './d3/views/shared/bar-chart-vertical/bar-chart-vertical.component';

@NgModule({
    declarations: [
        ContentComponent,
        ContentMenuComponent,
        ContentComponent,
        ContentHostDirective,
        ForceDirectedGraphComponent,
        MenuElementComponent,
        UsZoomableComponent,
        DraggableDirective,
        DraggableTreeDirective,
        ZoomableDirective,
        GraphLinkComponent,
        GraphNodeComponent,
        GraphComponent,
        MapUsComponent,
        RectangleComponent,
        GeoPathComponent,
        UsaStaticComponent,
        ZoomableToBoundingDirective,
        BoundingZoomResetDirective,
        ZoomByPercentageDirective,
        ForceDirectedTreeComponent,
        HierarchyTreeComponent,
        TreeNodeComponent,
        ForceDirectedGraphExternalComponent,
        TreeGraphComponent,
        GraphLinkPathComponent,
        SystemMessageComponent,
        NodeContentComponent,
        DynamicComponentLoaderComponent,
        BarChartVerticalComponent,
    ],
    entryComponents: [
        SystemMessageComponent
    ],
    imports: [
        CommonModule,
        ContentRoutingModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatToolbarModule,
    ]
})
export class ContentModule {
}
