import {
    Component,
    AfterViewInit,
    ComponentFactoryResolver,
    ElementRef,
    OnChanges,
    OnInit,
    SimpleChanges,
    ViewChild,
    Input
} from '@angular/core';
import { ContentHostDirective } from '../../../../../directives/content-host.directive';
import { DynamicComponentType } from './dynamic-component-type';

@Component({
    selector: 'app-dynamic-component-loader',
    templateUrl: './dynamic-component-loader.component.html',
    styleUrls: ['./dynamic-component-loader.component.scss']
})
export class DynamicComponentLoaderComponent implements AfterViewInit, OnChanges, OnInit {
    @Input('dynamicComponents') dynamicComponents: DynamicComponentType;
    @ViewChild(ContentHostDirective, {static: true}) componentHost: ContentHostDirective;
    @ViewChild('dynamicComponentLoader', {static: true}) dynamicComponentLoader: ElementRef;

    // @ViewChild('dynamicContainer', {static: true}) dynamicContainer: ElementRef;

    constructor(/*ööööööööööööôööööööööööööö*/
                private componentFactoryResolver: ComponentFactoryResolver,
                /*ööööööööööööôööööööööööööö*/) {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: DynamicComponentLoaderComponent ngOnChanges :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::changes::', changes,
            '\n::changes.dynamicComponents.currentValue::', changes.dynamicComponents.currentValue,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        if ( typeof changes.dynamicComponents.currentValue !== 'undefined' ) {
            if ( this.dynamicComponents[0].component !== undefined ) {
                this.loadDynamicComponent();
            }
        } else if ( !changes.dynamicComponents.currentValue ) {
            this.removeDynamicComponent(null);
        }
    }

    private loadDynamicComponent(): void {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.dynamicComponents[0].component);
        const viewContainerRef = this.componentHost.viewContainerRef;
        viewContainerRef.clear();
        const componentRef = viewContainerRef.createComponent(componentFactory);
        (<DynamicComponentType>componentRef.instance).data = this.dynamicComponents[0].data;

        console.log(
            '\n::::::::::::::::::::::::::::::::::::: DynamicComponentLoaderComponent loadDynamicComponent ::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.componentHost::', this.componentHost,
            '\n::this.componentHost.viewContainerRef::', this.componentHost.viewContainerRef,
            '\n::this.componentHost.viewContainerRef.element::', this.componentHost.viewContainerRef.element,
            '\n::this.componentHost.viewContainerRef.element.nativeElement::', this.componentHost.viewContainerRef.element.nativeElement,
            '\n::componentRef::', componentRef,
            '\n::componentFactory::', componentFactory,
            '\n::viewContainerRef::', viewContainerRef,
            '\n::this.dynamicComponents::', this.dynamicComponents,
            '\n::this.dynamicComponents[0].component::', this.dynamicComponents[0].component,
            '\n::this.dynamicComponents[0].data::', this.dynamicComponents[0].data,
            '\n::::::::::::::::::::::::::::::::::::::      ::::::::      :::::::::::::::::::::::::::::::::::::::::::::::::::',
        );
    }

    private removeDynamicComponent(index: number) {
        const viewContainerRef = this.componentHost.viewContainerRef;
        viewContainerRef.clear();
        console.log(
            '\n::::::::::::::::::::::::::::::::::::: DynamicComponentLoaderComponent removeDynamicComponent ::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::viewContainerRef::', viewContainerRef,
            '\n::::::::::::::::::::::::::::::::::::::      ::::::::      :::::::::::::::::::::::::::::::::::::::::::::::::::',
        );
    }

}
