import { Type } from '@angular/core';

export class DynamicComponentType {



    constructor(
        /*{[(|/0000000OOoOO000000\|)]}*/
        public component: Type<any>,
        public data: any
        /*{[(|/0000000OOoOO000000\|)]}*/) {

/*                console.log(
                    '\n::::::::::::::::::::::::::::::::::::: DynamicComponentType constructor ::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::this.component::', this.component,
                    '\n::this.data::', this.data,
                    '\n::::::::::::::::::::::::::::::::::::::      ::::::::      :::::::::::::::::::::::::::::::::::::::::::::::::::',
                );*/
    }
}
