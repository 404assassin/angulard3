import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { GraphNode } from '../../../models/graph-node.model';

@Component({
    selector: 'g[app-graph-node], app-graph-node',
    templateUrl: './graph-node.component.html',
    styleUrls: ['./graph-node.component.scss']
})
export class GraphNodeComponent implements AfterViewInit, OnInit {
    @Input() node: GraphNode;
    @Input() currentNode: GraphNode;
    @Input() index: number;

    constructor() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNodeComponent constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.node::', this.node,
        //     '\n::this.node.color::', this.node.color,
        //     '\n::this.node.r::', this.node.r,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    ngOnInit() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNodeComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.currentNode::', this.currentNode,
        //     '\n::this.currentNode[0]::', this.currentNode[0],
        //     '\n::this.currentNode[this.index]::', this.currentNode[this.index],
        //     '\n::this.node::', this.node,
        //     '\n::this.node.color::', this.node.color,
        //     '\n::this.node.fontSize::', this.node.fontSize,
        //     '\n::this.node.r::', this.node.r,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    ngAfterViewInit() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNodeComponent AfterViewInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.node::', this.node,
        //     '\n::this.node.color::', this.node.color,
        //     '\n::this.node.r::', this.node.r,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public routeUpdate(){

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: GraphNodeComponent routeUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.node::', this.node,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

}
