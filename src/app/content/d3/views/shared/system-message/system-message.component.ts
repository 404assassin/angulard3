import { Component, Input } from '@angular/core';
import { NodeContent } from '../node-content/node-content.interface';

@Component({
    selector: 'app-system-message',
    templateUrl: './system-message.component.html',
    styleUrls: ['./system-message.component.scss']
})
export class SystemMessageComponent implements NodeContent {

    @Input() data: any;

    constructor() {
        console.log(
            '\n::::::::::::::::::::::::::::::::::::: SystemMessageComponent constructor ::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.data::', this.data,
            '\n::::::::::::::::::::::::::::::::::::::      ::::::::      :::::::::::::::::::::::::::::::::::::::::::::::::::',
        );
    }

}
