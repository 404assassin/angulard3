import { Component, Input, OnInit } from '@angular/core';
import { Rectangle } from '../../../models/rectangle.model';

@Component({
    selector: 'g[app-rectangle], app-rectangle',
    templateUrl: './rectangle.component.html',
    styleUrls: ['./rectangle.component.scss']
})
export class RectangleComponent implements OnInit {
    @Input() options: Rectangle;

    constructor() {
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: RectangleComponent ; :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::options::', this.options,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

    ngOnInit() {
    }

}
