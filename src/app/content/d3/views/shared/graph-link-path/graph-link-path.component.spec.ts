import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphLinkPathComponent } from './graph-link-path.component';

describe('GraphLinkPathComponent', () => {
  let component: GraphLinkPathComponent;
  let fixture: ComponentFixture<GraphLinkPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphLinkPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphLinkPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
