import { Component, Input, KeyValueDiffers, KeyValueDiffer, OnInit, SimpleChanges } from '@angular/core';
// import { GraphLink } from '../../../models/graph-link.model';
import { GraphLinkPath } from '../../../models/graph-link-path.model';

@Component({
    selector: 'g[app-graph-link-path], app-graph-link-path',
    templateUrl: './graph-link-path.component.html',
    styleUrls: ['./graph-link-path.component.scss']
})
export class GraphLinkPathComponent implements OnInit {
    @Input() link: GraphLinkPath;
    @Input() index: number;
    private attrD ?: string | any;
    private differ: KeyValueDiffer<string, any>;
    private previousValueX: number;

    constructor(private linkUpdate: KeyValueDiffers) {
        this.differ = this.linkUpdate.find({}).create();
    }

    ngOnInit() {

        // this.attrD = 'M' + this.link.source['x'] + ',' + this.link.source['y'] + 'L' + this.link.target['x'] + ',' + this.link.target['y'];
        // this.reflowLinkLabel();
        //         this.attrD = 'M' + this.link.source['x'] + ',' + this.link.source['y'] + 'L' + this.link.target['x'] + ',' + this.link.target['y'];
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLinkPathComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.link::', this.link,
        //     '\n::this.differ::', this.differ,
        //     '\n::this.link.stroke::', this.link.stroke,
        //     '\n::\'M\' + this.link.source[\'x\'] + \',\' + this.link.source[\'y\'] + \'L\' + this.link.target[\'x\'] + \',\' + this.link.target[\'y\']::', 'M' + this.link.source['x'] + ',' + this.link.source['y'] + 'L' + this.link.target['x'] + ',' + this.link.target['y'],
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

    }

    ngDoCheck() {
        if ( typeof this.link.linkLabel !== 'undefined' ) {
            if ( this.previousValueX !== this.link.source['x'] ) {
                this.previousValueX = this.link.source['x'];
                this.reflowLinkLabel();
            }
        } else {
            this.attrD = 'M' + this.link.source['x'] + ',' + this.link.source['y'] + 'L' + this.link.target['x'] + ',' + this.link.target['y'];
        }
    }

    private reflowLinkLabel() {
        let reverseLabel: number = this.link.target['x'] - this.link.source['x'];
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLinkPathComponent ngDoCheck :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.attrD::', this.attrD,
        //     '\n::reverseLabel::', reverseLabel,
        //     '\n::reverseLabel <= 0::', reverseLabel <= 0,
        //     '\n::reverseLabel > 0::', reverseLabel > 0,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( reverseLabel > 0 ) {
            this.attrD = 'M' + this.link.source['x'] + ',' + this.link.source['y'] + 'L' + this.link.target['x'] + ',' + this.link.target['y'];
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: GraphLinkPathComponent ngDoCheck :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::this.attrD::', this.attrD,
            //     '\n::this.link.linkLabel::', this.link.linkLabel,
            //     '\n::this.link.target[\'x\'] - this.link.source[\'x\']::', this.link.target['x'] - this.link.source['x'],
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );
        } else if ( reverseLabel <= 0 ) {
            this.attrD = 'M' + this.link.target['x'] + ',' + this.link.target['y'] + 'L' + this.link.source['x'] + ',' + this.link.source['y'];
        }
    }

}
