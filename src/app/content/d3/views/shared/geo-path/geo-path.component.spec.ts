import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoPathComponent } from './geo-path.component';

describe('GeoPathComponent', () => {
  let component: GeoPathComponent;
  let fixture: ComponentFixture<GeoPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeoPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
