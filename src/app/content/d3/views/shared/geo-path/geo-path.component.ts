import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GraphLink } from '../../../models/graph-link.model';
import { GeoPath } from '../../../models/geo-path.model';
import { GeoService } from '../../../../../services/d3/geo.service';
import { D3Service } from '../../../../../services/d3/d3.service';

import * as d3 from 'd3';

@Component({
    selector: 'g[app-geo-path], app-geo-path',
    // selector: 'g[app-geo-path], app-geo-path',
    templateUrl: './geo-path.component.html',
    styleUrls: ['./geo-path.component.scss']
})
export class GeoPathComponent implements OnInit {
    @Input() pathData: GeoPath;
    @Input() elementIndex: number;
    @Output() open: EventEmitter<any> = new EventEmitter();
    public path: any;
    private translateParams: any = {width: 960, height: 500}
    public projection: any = this.geoService.getProjectionGeoAldersUSA(1000, this.translateParams);
    public geoPath: any = this.geoService.getGeoPath(this.projection);

    constructor(/*<<HHHHHHHHHHH|HHHHHHHHHHH>>*/
                private d3Service: D3Service,
                private elementRef: ElementRef,
                private geoService: GeoService,
                /*<<HHHHHHHHHHH|HHHHHHHHHHH>>*/) {
    }

    ngOnInit() {
        this.path = this.geoService.getGeoAldersUSA(this.pathData, 1000, this.translateParams);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GeoPathComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.pathData::', this.pathData,
        //     '\n::this.pathData.id::', this.pathData['id'],
        //     // '\n::this.path::', this.path,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public pathSelection(): void {
        const selectedState: any = this.elementRef.nativeElement.getElementsByTagName('path')[0];
        const projection: any = this.geoService.getProjectionGeoAldersUSA(1000, this.translateParams);
        const path: any = this.geoService.getGeoPath(projection);
        /*        console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: GeoPathComponent pathSelection :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::projection::', projection,
                    '\n::path::', path,
                    // '\n::pathData::', pathData,
                    '\n::this.geoService.projectionGeoAldersUSA::', this.geoService.projectionGeoAldersUSA,
                    '\n::this.elementRef.nativeElement::', this.elementRef.nativeElement,
                    '\n::this.elementRef.nativeElement.getElementsByClassName(\'state-path\')::', this.elementRef.nativeElement.getElementsByClassName('state-path')[0],
                    '\n::this.elementRef.nativeElement.getElementsByTagName(\'path\')::', this.elementRef.nativeElement.getElementsByTagName('path')[0],
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );*/
    }

}
