import { Component, Input, OnInit } from '@angular/core';
import { GraphLink } from '../../../models/graph-link.model';

@Component({
    selector: 'g[app-graph-link], app-graph-link',
    templateUrl: './graph-link.component.html',
    styleUrls: ['./graph-link.component.scss']
})
export class GraphLinkComponent implements OnInit {
    @Input() link: GraphLink;

    constructor() {
    }

    ngOnInit() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLinkComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.link::', this.link,
        //     '\n::this.link.stroke::', this.link.stroke,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

}
