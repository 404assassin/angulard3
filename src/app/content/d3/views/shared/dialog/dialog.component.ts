import {
    Component,
    AfterViewInit,
    ChangeDetectorRef,
    ComponentRef,
    ComponentFactoryResolver,
    OnDestroy,
    Type,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import { DialogInsertionDirective } from '../../../../../directives/dialog-insertion.directive';

@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements AfterViewInit, OnDestroy {

    // private readonly _onClose = new Subject<any>()

    public childComponentType: Type<any>;
    public componentRef: ComponentRef<any>
    // public onClose = this._onClose.asObservable()

    @ViewChild(DialogInsertionDirective, {static: true}) insertionPoint: DialogInsertionDirective;

    constructor(/*ōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōō*/
                private componentFactoryResolver: ComponentFactoryResolver,
                private changeDetectorRef: ChangeDetectorRef,
                /*ōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōō*/) {

    }

    ngAfterViewInit() {
        this.loadChildComponent(this.childComponentType);
        this.changeDetectorRef.detectChanges();
    }

    ngOnDestroy() {
        if ( this.componentRef ) {
            this.componentRef.destroy();
        }
    }

    onOverlayClicked(evt: MouseEvent) {
        // close the dialog
    }

    onDialogClicked(evt: MouseEvent) {
        evt.stopPropagation();
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: DialogComponent onDialogClicked :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::evt::', evt,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

    loadChildComponent(componentType: Type<any>) {
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType);

        let viewContainerRef = this.insertionPoint.viewContainerRef;
        viewContainerRef.clear();

        this.componentRef = viewContainerRef.createComponent(componentFactory);
    }

}
