import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from './dialog.component';
import { DialogErrorComponent } from './dialog-error/dialog-error.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        DialogComponent,
        DialogErrorComponent
    ],
    entryComponents: [
        DialogComponent
    ],
})
export class DialogModule {
}
