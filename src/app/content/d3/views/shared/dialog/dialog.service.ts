import {
    Injectable,
    ComponentFactoryResolver,
    ApplicationRef,
    Injector,
    EmbeddedViewRef,
    ComponentRef,
    Type
} from '@angular/core';
import { DialogModule } from './dialog.module';
import { DialogComponent } from './dialog.component';

@Injectable({
    providedIn: DialogModule,
})
export class DialogService {
    dialogComponentRef: ComponentRef<DialogComponent>

    constructor(/*õõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõ*/
                private componentFactoryResolver: ComponentFactoryResolver,
                private applicationRef: ApplicationRef,
                private injector: Injector
                /*õõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõõ*/) {
    }

    appendDialogComponentToBody() {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DialogComponent);
        const componentRef = componentFactory.create(this.injector);
        this.applicationRef.attachView(componentRef.hostView);

        const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
        document.body.appendChild(domElem);

        this.dialogComponentRef = componentRef;

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: DialogService appendDialogComponentToBody :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

    private removeDialogComponentFromBody() {
        this.applicationRef.detachView(this.dialogComponentRef.hostView);
        this.dialogComponentRef.destroy();
    }

    public open(componentType: Type<any>) {
        this.appendDialogComponentToBody();

        this.dialogComponentRef.instance.childComponentType = componentType;
    }
}
