import { Component, ComponentFactoryResolver, Input, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ContentHostDirective } from '../../../../../directives/content-host.directive';
import { GraphNode } from '../../../models/graph-node.model';
import { GraphLinkPath } from '../../../models/graph-link-path.model';

@Component({
    selector: 'app-node-content',
    templateUrl: './node-content.component.html',
    styleUrls: ['./node-content.component.scss']
})
export class NodeContentComponent implements OnInit, OnDestroy {
    @Input() nodes: GraphNode[];
    nodeItem:any;
    currentAdIndex = -1;
    @ViewChild(ContentHostDirective, {static: true}) adHost: ContentHostDirective;
    interval: any;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    }

    ngOnInit() {

        // this.nodeItem.push(new CardItem(SystemMessageComponent, {name: 'Card Dynamisch'}));
        // this.loadComponent();
        // this.getAds();
        // this.loadCards();
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: NodeContentComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.nodes::', this.nodes,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }

    loadComponent() {
        let test

        // link = new GraphLinkPath(source, target);
/*        this.currentAdIndex = (this.currentAdIndex + 1) % this.nodes.length;
        const nodeItem = this.nodes[this.currentAdIndex];

        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(nodeItem.component);
        // const componentFactory = this.componentFactoryResolver.resolveComponentFactory(new SystemMessageComponent());

        const viewContainerRef = this.adHost.viewContainerRef;
        viewContainerRef.clear();
        const componentRef = viewContainerRef.createComponent(componentFactory);*/
        // (<NodeContentComponent>componentRef.instance).data = nodeItem.data;

    }
    // entryComponents: [ SystemMessageComponent, HeroProfileComponent ],

    // getAds() {
        // this.interval = setInterval(() => {
        //     this.loadComponent();
        // }, 3000);
    // }

/*    loadCards() {
        const viewContainerRef = this.appCards.viewContainerRef;
        viewContainerRef.clear();
        for (const card of this.cards) {
            const componentFactory = this.componentFactoryResolver.resolveComponentFactory(card.component);
            const componentRef = viewContainerRef.createComponent<CardContentComponent>(componentFactory);
            componentRef.instance.data = card.data;
            componentRef.instance.delete.subscribe(() => {
                // handle delete logic
            });
        }
    }*/
}
