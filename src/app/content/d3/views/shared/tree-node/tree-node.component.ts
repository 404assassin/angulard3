import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'g[app-tree-node], app-tree-node',
    templateUrl: './tree-node.component.html',
    styleUrls: ['./tree-node.component.scss']
})
export class TreeNodeComponent implements OnInit {
    @Input('items') public items: any;
    @Input('routerLink') public routerLink: any;

    constructor() {
    }

    ngOnInit() {
      console.log(
          '\n:::::::::::::::::::::::::::::::::::::: TreeNodeComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
          '\n::this::', this,
          '\n::this.routerLink::', this.routerLink,
          '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
      );
    }

}
