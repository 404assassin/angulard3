import { Component, OnInit } from '@angular/core';
import { GeoUsService } from '../../../../../../services/geo-us.service';
import { UsStatesService } from '../../../../../../services/us-states.service';
import { environment } from '../../../../../../../environments/environment';

@Component({
    selector: 'app-us-zoomable',
    templateUrl: './us-zoomable.component.html',
    styleUrls: ['./us-zoomable.component.scss']
})
export class UsZoomableComponent implements OnInit {
    private geoDatas: any;
    private statesDatas: Array<object>;
    public combindDatas: Array<object>;

    constructor(/*==================**==================*/
                private geoUsService: GeoUsService,
                private usStatesService: UsStatesService,
                /*==================**==================*/) {
    }

    ngOnInit() {
        console.log(
            '\n::::::::::::::::::::::::::::::::  UsZoomableComponent  ngOnInit  ::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        this.getData();
        this.getStatesData();
    }

    public getData(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: UsZoomableComponent getData :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.geoUsService.getDatas().subscribe(
            (data) => {
                this.geoDatas = data;
                console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: UsZoomableComponent getData :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::data::', data,
                    '\n::this.geoDatas::', this.geoDatas,
                    '\n::this.geoDatas[\'objects\'].states.geometries::', this.geoDatas['objects'].states.geometries,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            (error) => {
                console.error(
                    '\n::::::::::::::::::::::::::::::::::::::  UsZoomableComponent  GET  Error  ::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::POST Request Error ::', error,
                    '\n::POST Request Error Message ::', error.message,
                    '\n::POST Request Error Message ::', error.error.message,
                    '\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            () => this.dataUpdate(this.geoDatas)
        );
    }

    public getStatesData(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: UsZoomableComponent getStatesData :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.usStatesService.getDatas().subscribe(
            (data) => {
                this.statesDatas = data;
                console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: UsZoomableComponent getStatesData :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::data::', data,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            (error) => {
                console.error(
                    '\n::::::::::::::::::::::::::::::::::::::  UsZoomableComponent  GET  Error  ::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::POST Request Error ::', error,
                    '\n::POST Request Error Message ::', error.message,
                    '\n::POST Request Error Message ::', error.error.message,
                    '\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            () => this.dataUpdate(this.geoDatas)
        );
    }

    private dataUpdate(data): void {
        let localData: any;
        let localData2: any;
        // localData = this.statesDatas[0]['us-states'];
        console.log(
            '\n::::::::::::::::::: UsZoomableComponent dataUpdate !!this.statesDatas && !!this.geoDatas 0 :::::::::::::::::::',
            '\n::this::', this,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        if ( (typeof this.statesDatas !== 'undefined' && this.statesDatas[0]['us-states'] && this.statesDatas[0]['us-states'].length) && (typeof this.geoDatas !== 'undefined' && this.geoDatas['objects'].states.geometries && this.geoDatas['objects'].states.geometries.length) ) {
            // if ( !!this.statesDatas && !!this.geoDatas ) {
            // localData
            // localData = [...this.statesDatas[0]['us-states'], ...this.geoDatas['objects'].states.geometries];
            // localData2.push(...data); = [ ...this.geoDatas['objects'].states.geometries, ...this.statesDatas[0]['us-states']];

            /*            localData = this.statesDatas[0]['us-states'].map((a)=>{
                            let obj2 = this.geoDatas['objects'].states.geometries].find((b)=> a.name === b.name);
                            if(obj2)
                                Object.assign(a,obj2);
                            return a;
                        });*/

            /*this.statesDatas[0]['us-states'].forEach((itm, i) => {
                this.combindDatas.push(Object.assign({}, itm, this.geoDatas['objects'].states.geometries[i]));
            });*/

            // this.deliniatedData = [];
            // this.subscribersCount = [];
            // this.membersCount = [];
            // this.monthlyCurrentCount = [];
            // this.monthlyRenewalCount = [];
            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: UsZoomableComponent dataUpdate !!this.statesDatas && !!this.geoDatas 1:::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::this::', this,
                '\n::this.geoDatas[\'objects\'].states.geometries.length::', this.geoDatas['objects'].states.geometries.length,
                '\n::this.statesDatas[0][\'us-states\'].length::', this.statesDatas[0]['us-states'].length,
/*                '\n::this.statesDatas[0][\'us-states\']::', this.statesDatas[0]['us-states'],
                '\n::this.geoDatas.length::', this.geoDatas.length,
                '\n::!!this.statesDatas::', !!this.statesDatas,
                '\n::!!this.geoDatas::', !!this.geoDatas,*/
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );
            for (let i = 0; i < this.geoDatas['objects'].states.geometries.length; i = i + 1) {
/*                console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: UsZoomableComponent dataUpdate !!this.statesDatas && !!this.geoDatas 1:::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::this.statesDatas[0][\'us-states\'][i]::', this.statesDatas[0]['us-states'][i],
                    '\n::this.geoDatas.objects.states.geometries[i]::', this.geoDatas.objects.states.geometries[i],
                    '\n::typeof this.statesDatas[0][\'us-states\'][i] !== \'undefined\'::', typeof this.statesDatas[0]['us-states'][i] !== 'undefined',
                    '\n::typeof this.geoDatas.objects.states.geometries[i] !== \'undefined\'::', typeof this.geoDatas.objects.states.geometries[i] !== 'undefined',
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );*/
                /*// if ( typeof this.statesDatas[0]['us-states'][i] !== 'undefined' ) {
                    this.combindDatas.push(this.statesDatas[0]['us-states'][i]);
                // }
                this.combindDatas.push(this.geoDatas['objects'].states.geometries[i]);*/
                this.geoDatas['objects'].states.geometries[i].stateInfo = (this.statesDatas[0]['us-states'][i]);
                // this.combindDatas.push(this.geoDatas['objects'].states.geometries[i]);
            }
            this.combindDatas =  JSON.parse(JSON.stringify(this.geoDatas));
            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: UsZoomableComponent dataUpdate !!this.statesDatas && !!this.geoDatas 2:::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::this::', this,
                // '\n::this.statesDatas.length::', this.statesDatas.length,
                // '\n::this.statesDatas::', this.statesDatas,
                // '\n::!!this.statesDatas::', !!this.statesDatas,
                // '\n::this.geoDatas.length::', this.geoDatas.length,
                '\n::this.geoDatas::', this.geoDatas,
                '\n::this.combindDatas::', this.combindDatas,
                // '\n::!!this.geoDatas::', !!this.geoDatas,
                // '\n::this.combindDatas::', this.combindDatas,
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );
        }

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: UsZoomableComponent dataUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
        //     '\n::data::', data,
        //     '\n::this.statesDatas::', this.statesDatas,
        //     '\n::!!this.statesDatas::', !!this.statesDatas,
        //     '\n::this.geoDatas::', this.geoDatas,
        //     '\n::!!this.geoDatas::', !!this.geoDatas,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        // d3.json("/mbostock/raw/4090846/us.json", function (error, us) {
        //   if ( error ) throw error;

    }
}
