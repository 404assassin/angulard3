import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsZoomableComponent } from './us-zoomable.component';

describe('UsZoomableComponent', () => {
  let component: UsZoomableComponent;
  let fixture: ComponentFixture<UsZoomableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsZoomableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsZoomableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
