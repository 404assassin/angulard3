import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapUsComponent } from './map-us.component';

describe('MapUsComponent', () => {
  let component: MapUsComponent;
  let fixture: ComponentFixture<MapUsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapUsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
