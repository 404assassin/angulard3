import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef,
    Input,
    OnInit,
    QueryList,
    ViewChild,
    ViewChildren
} from '@angular/core';

import { D3Service } from '../../../../../../../services/d3/d3.service';
import { GeoService } from '../../../../../../../services/d3/geo.service';
import { DraggableService } from '../../../../../../../services/gsap/draggable.service';

import * as d3 from 'd3';
import * as topojson from "topojson-client";
import { Power2, TweenMax } from 'gsap/all';

@Component({
    selector: 'app-map-us',
    templateUrl: './map-us.component.html',
    styleUrls: ['./map-us.component.scss']
})
export class MapUsComponent implements AfterViewInit, OnInit {
    // @Input() items: MenuElement[];
    @Input() geoDatas: Array<any>;
    @ViewChild('svgReference', {static: true}) svgReference: ElementRef;
    @ViewChild('plusIcon', {static: true}) plusIcon: ElementRef;
    @ViewChild('minusIcon', {static: true}) minusIcon: ElementRef;
    @ViewChild('resetIcon', {static: true}) resetIcon: ElementRef;
    @ViewChild('contentPopUp', {static: true}) contentPopUp: ElementRef;
    @ViewChildren('elementPath') elementPath: QueryList<ElementRef>;
    public translateParams: any = {width: 960, height: 500}
    public projection: any = this.geoService.getProjectionGeoAldersUSA(1000, this.translateParams);
    public geoPath: any = this.geoService.getGeoPath(this.projection);
    public windowOptions: { width, height } = {width: 960, height: 500};
    public rectangleOptions: { width, height } = {width: 960, height: 500};
    public viewBoxParameters: any = '0 0 ' + this.windowOptions.width + ' ' + this.windowOptions.height;
    private width: number = 960;
    private height: number = 500;
    private usaGeoData: any;
    public pathDatas: any;
    public pathData8: any;
    public elementPath8: any = {};
    private path: any;
    public selectedIndex: number;
    public pathData8Ref: any;
    public zoomIconStates: any = {
        zoom: false,
        minus: true,
        reset: true
    };

    constructor(/*HHHHHHHHHHHHH::HHHHHHHHHHHHH*/
                private changeDetectorRef: ChangeDetectorRef,
                private d3Service: D3Service,
                private geoService: GeoService,
                private draggableService: DraggableService,
                private elementRef: ElementRef,
                /*HHHHHHHHHHHHH::HHHHHHHHHHHHH*/) {
    }

    ngOnInit(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.geoDatas::', this.geoDatas,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.draggableService._onDragStartObs.subscribe((_onDragStart) => {
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::_onDragStart::', _onDragStart,
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );
            this.resetEvent(null);
        });

        this.initOnStateData();
    }

    ngAfterViewInit(): void {
        this.elementPath8 = this.elementPath.toArray()[8];
        this.changeDetectorRef.detectChanges();
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent ngAfterViewInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.elementPath::', this.elementPath,
        //     '\n::this.svgReference::', this.svgReference,
        //     '\n::this.elementPath::', this.elementPath,
        //     '\n::this.elementPath8::', this.elementPath8,
        //     '\n::this.elementPath.toArray()[8]::', this.elementPath.toArray()[8],
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    private initOnStateData(): void {
        this.pathDatas = topojson.feature(this.geoDatas, this.geoDatas['objects'].states).features;
        this.pathData8 = this.pathDatas[8];

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent initOnStateData :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.geoDatas::', this.geoDatas,
        //     '\n::this.pathDatas::', this.pathDatas,
        //     '\n::this.pathData8::', this.pathData8,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public trackByFn(index, item) {
        //     console.log(
        //         '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent trackByFn :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //         '\n::this::', this,
        //         '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        //     );
        return item.id; // unique id corresponding to the item
    }

    public selectedDC($eventValue: any): void {
        this.selectedStateAnimateOn(8);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent selectedDC :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$eventValue::', $eventValue,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public selectedState($eventValue: any): void {
        this.selectedStateAnimateOn($eventValue);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent selectedState :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$eventValue::', $eventValue,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public resetEvent($eventValue: any): void {
        this.selectedIndex = $eventValue;
        this.geoService.resetPathStroke(this.svgReference.nativeElement);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent resetEvent :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$eventValue::', $eventValue,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    private selectedStateAnimateOn($eventValue: any): void {
        this.selectedIndex = $eventValue;
        TweenMax.set(this.contentPopUp.nativeElement, {
            scale: 0.70,
            autoAlpha: 0.0,
        });

        TweenMax.to(this.contentPopUp.nativeElement, 0.85, {
            scale: 1.0,
            autoAlpha: 0.99,
            ease: Power2.easeOut
        });
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: MapUsComponent selectedStateAnimateOn :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$eventValue::', $eventValue,
        //     '\n::this.contentPopUp::', this.contentPopUp,
        //     '\n::this.svgReference::', this.svgReference,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public setPlusState(clickTarget: any) {

        TweenMax.to(this.plusIcon.nativeElement, 0.75, {
            autoAlpha: 0.50,
            pointerEvents: 'none',
            ease: Power2.easeInOut,
        });

        TweenMax.to(this.minusIcon.nativeElement, 0.75, {
            autoAlpha: 1.0,
            pointerEvents: 'inherit',
            ease: Power2.easeInOut,
        });

        TweenMax.to(this.resetIcon.nativeElement, 0.75, {
            autoAlpha: 1.0,
            pointerEvents: 'inherit',
            ease: Power2.easeInOut,
        });
        /*console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ZoomByPercentageDirective setPlusState :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::clickTarget::', clickTarget,
            '\n::clickTarget.id::', clickTarget.id,
            '\n::this.zoomIconStates::', this.zoomIconStates,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/
    }

    public setMinusState(clickTarget: any) {

        TweenMax.to(this.plusIcon.nativeElement, 0.75, {
            autoAlpha: 1.0,
            pointerEvents: 'inherit',
            ease: Power2.easeInOut,
        });

        TweenMax.to(this.minusIcon.nativeElement, 0.75, {
            autoAlpha: 0.50,
            pointerEvents: 'none',
            ease: Power2.easeInOut,
        });

        TweenMax.to(this.resetIcon.nativeElement, 0.75, {
            autoAlpha: 1.0,
            pointerEvents: 'inherit',
            ease: Power2.easeInOut,
        });
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ZoomByPercentageDirective setMinusState :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::clickTarget::', clickTarget,
            '\n::clickTarget.id::', clickTarget.id,
            '\n::this.zoomIconStates::', this.zoomIconStates,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

    public setResetState(clickTarget: any) {

        TweenMax.to(this.plusIcon.nativeElement, 0.75, {
            autoAlpha: 1.0,
            pointerEvents: 'inherit',
            ease: Power2.easeInOut,
        });

        TweenMax.to(this.minusIcon.nativeElement, 0.75, {
            autoAlpha: 0.50,
            pointerEvents: 'none',
            ease: Power2.easeInOut,
        });

        TweenMax.to(this.resetIcon.nativeElement, 0.75, {
            autoAlpha: 0.50,
            pointerEvents: 'none',
            ease: Power2.easeInOut,
        });
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ZoomByPercentageDirective setResetState :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::clickTarget::', clickTarget,
            '\n::clickTarget.id::', clickTarget.id,
            '\n::this.zoomIconStates::', this.zoomIconStates,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }
}
