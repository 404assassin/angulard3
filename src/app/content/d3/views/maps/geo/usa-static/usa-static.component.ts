import { Component, OnInit } from '@angular/core';
import { GeoUsService } from '../../../../../../services/geo-us.service';

@Component({
  selector: 'app-usa-static',
  templateUrl: './usa-static.component.html',
  styleUrls: ['./usa-static.component.scss']
})
export class UsaStaticComponent implements OnInit {
  public geoDatas: Array<object>;

  constructor(/*==================**==================*/
              private geoUsService: GeoUsService,
              /*==================**==================*/) {
  }

  ngOnInit() {
    console.log(
        '\n::::::::::::::::::::::::::::::::  UsaStaticComponent  ngOnInit  ::::::::::::::::::::::::::::::::::::::::::',
        '\n::this::', this,
        '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
    );
    this.getData();
  }

  public getData(): void {
    // console.log(
    //     '\n:::::::::::::::::::::::::::::::::::::: UsaStaticComponent dataUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
    //     '\n::this::', this,
    //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
    // );
    this.geoUsService.getDatas().subscribe(
        (data) => {
          this.geoDatas = data;
          console.log(
              '\n:::::::::::::::::::::::::::::::::::::: UsaStaticComponent getData :::::::::::::::::::::::::::::::::::::::::::::::::::',
              '\n::this::', this,
              '\n::data::', data,
              '\n::this.geoDatas::', this.geoDatas,
              '\n::this.geoDatas[\'objects\'].states.geometries::', this.geoDatas['objects'].states.geometries,
              '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
          );
        },
        (error) => {
          console.error(
              '\n::::::::::::::::::::::::::::::::::::::  UsaStaticComponent  GET  Error  ::::::::::::::::::::::::::::::::::::::',
              '\n::this::', this,
              '\n::POST Request Error ::', error,
              '\n::POST Request Error Message ::', error.message,
              '\n::POST Request Error Message ::', error.error.message,
              '\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
          );
        },
        () => this.dataUpdate(this.geoDatas)
    );
  }

  private dataUpdate(data): void {
    // const localData: any = data;

    console.log(
        '\n:::::::::::::::::::::::::::::::::::::: UsaStaticComponent dataUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
        '\n::this::', this,
        '\n::data::', data,
        '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
    );
    // d3.json("/mbostock/raw/4090846/us.json", function (error, us) {
    //   if ( error ) throw error;

  }
}
