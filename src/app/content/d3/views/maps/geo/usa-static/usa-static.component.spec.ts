import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsaStaticComponent } from './usa-static.component';

describe('UsaStaticComponent', () => {
  let component: UsaStaticComponent;
  let fixture: ComponentFixture<UsaStaticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsaStaticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsaStaticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
