import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForceDirectedGraphExternalComponent } from './force-directed-graph-external.component';

describe('ForceDirectedGraphExternalComponent', () => {
  let component: ForceDirectedGraphExternalComponent;
  let fixture: ComponentFixture<ForceDirectedGraphExternalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForceDirectedGraphExternalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForceDirectedGraphExternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
