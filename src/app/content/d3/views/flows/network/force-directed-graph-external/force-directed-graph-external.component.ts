import { Component, OnInit } from '@angular/core';
import { GraphNode } from '../../../../models/graph-node.model';
import { GraphLink } from '../../../../models/graph-link.model';
import { GraphLinkPath } from '../../../../models/graph-link-path.model';
import { ForceDirectedService } from '../../../../../../services/force-directed.service';

@Component({
    selector: 'app-force-directed-graph-external',
    templateUrl: './force-directed-graph-external.component.html',
    styleUrls: ['./force-directed-graph-external.component.scss']
})
export class ForceDirectedGraphExternalComponent implements OnInit {
    private nodeLength: number;
    private nodesData: any;
    public nodes: GraphNode[] = [];
    public links: GraphLinkPath[] = [];
    public loopFlag: boolean;

    constructor(/*SSSŠSSSSSS|Š|SSSSSSŠSSS*/
                private forceDirectedService: ForceDirectedService,
                /*SSSŠSSSSSS|Š|SSSSSSŠSSS*/) {
        this.getData();
    }

    ngOnInit(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphExternalComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.nodes::', this.nodes,
        //     '\n::this.links::', this.links,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public getData(): void {
        this.forceDirectedService.getDatas().subscribe(
            (data) => {
                this.nodesData = data.nodes;
                this.nodeLength = this.nodesData.length;
                // this.nodeLength = 151;
                this.links = data.links;
                console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphExternalComponent getData :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::data::', data,
                    '\n::this.nodes::', this.nodes,
                    '\n::this.nodeLength::', this.nodeLength,
                    '\n::this.nodes.length::', this.nodesData.length,
                    '\n::this.links::', this.links,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            (error) => {
                console.error(
                    '\n::::::::::::::::::::::::::::::::::::::  ForceDirectedGraphExternalComponent  GET  Error  ::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::POST Request Error ::', error,
                    '\n::POST Request Error Message ::', error.message,
                    '\n::POST Request Error Message ::', error.error.message,
                    '\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            () => this.dataUpdate()
        );
    }

    private dataUpdate(): void {
        // this.nodes = this.nodesData;
        this.nodeLength = this.nodesData.length;
        this.constructNodeArray();

    }

    /**
     * constructing the nodes array
     **/
    private constructNodeArray(): void {
        for (let i = 1; i <= this.nodeLength; i++) {
            // this.nodes[i].["color"] = "rgb(116,3,5)";
            this.nodes.push(new GraphNode(i));
            this.nodes[i - 1].color = this.nodesData[i - 1].color;
            this.nodes[i - 1].stroke = this.nodesData[i - 1].stroke;
            this.nodes[i - 1].strokeWidth = this.nodesData[i - 1].strokeWidth;
            this.nodes[i - 1].r = this.nodesData[i - 1].r;
            this.nodes[i - 1].route = this.nodesData[i - 1].route;
            this.nodes[i - 1].label = this.nodesData[i - 1].label;
            this.nodes[i - 1].fontSize = this.nodesData[i - 1].fontSize;
            this.nodes[i - 1].labelColor = this.nodesData[i - 1].labelColor;
            this.nodes[i - 1].displayTypes = this.nodesData[i - 1].displayTypes;
            // console.log(
            //     '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphExternalComponent constructNodeArray :::::::::::::::::::::::::::::::::::::::::::::::::::',
            //     '\n::this::', this,
            //     '\n::i::', i,
            //     '\n::new GraphNode(i)::', new GraphNode(i),
            //     '\n::this.nodes[i - 1]::', this.nodes[i - 1],
            //     '\n::this.nodeLength::', this.nodeLength,
            //     '\n::this.nodes.length::', this.nodes.length,
            //     '\n::this.nodesData[i - 1].length::', this.nodesData[i - 1].links.length,
            //     '\n::this.nodesData[i - 1].links.length > 0::', this.nodesData[i - 1].links.length > 0,
            //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            // );
            // if ( this.nodeLength === i ) {
            //     this.loopFlag = true;
            // }
            if ( this.nodesData[i - 1].links.length > 0 ) {
                this.constructLinksArray(i);
            } else {
                this.constructLink(i);
            }
        }
    }

    /**
     * get connections per node and store connection amount
     * create nodes links before starting the simulation
     **/
    private constructLinksArray(nodeIndex) {
        const getIndex = nodeNumber => nodeNumber - 1;
        const currentNode = this.nodesData[getIndex(nodeIndex)];
        let currentLinkIndex: number;
        if ( currentNode.links.length > 0 ) {
            for (let i = 1; i <= currentNode.links.length; i++) {
                this.nodes[nodeIndex - 1].linkCount++;

                this.links.push(new GraphLinkPath(nodeIndex, currentNode.links[i - 1].target));

                currentLinkIndex = this.links.length;
                this.links[currentLinkIndex - 1].linkLabel = currentNode.links[i - 1].linkLabel;
                this.links[currentLinkIndex - 1].linkLabelColor = currentNode.links[i - 1].linkLabelColor;
                this.links[currentLinkIndex - 1].stroke = currentNode.links[i - 1].stroke;
                this.links[currentLinkIndex - 1].strokeWidth = currentNode.links[i - 1].strokeWidth;
                this.links[currentLinkIndex - 1].strokeDashArray = currentNode.links[i - 1].strokeDashArray;

                if ( this.nodeLength === nodeIndex ) {
                    this.loopFlag = true;
                }
                // console.log(
                //     '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphExternalComponent constructLinksArray :::::::::::::::::::::::::::::::::::::::::::::::::::',
                //     '\n::this::', this,
                //     '\n::nodeIndex::', nodeIndex,
                //     '\n::i - 1::', i - 1,
                //     '\n::this.loopFlag::', this.loopFlag,
                //     '\n::currentNode.links[i - 1]::', currentNode.links[i - 1],
                //     '\n::currentNode.links[i - 1].stroke::', currentNode.links[i - 1].stroke,
                //     '\n::this.links::', this.links,
                //     '\n::this.links[i - 1]::', this.links[i - 1],
                //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                // );
            }
        } else {
            this.nodes[nodeIndex - 1].linkCount = 1;
            this.links.push(new GraphLinkPath(nodeIndex, 1));
        }
    }

    private constructLink(nodeIndex) {
        // const getIndex = nodeNumber => nodeNumber - 1;
        // const currentNode = this.nodesData[getIndex(nodeIndex)];
        this.nodes[nodeIndex - 1].linkCount = 1;
        // this.links.push(new GraphLink(nodeIndex, this.nodes[nodeIndex - 1]));
        if ( this.nodeLength === nodeIndex ) {
            this.loopFlag = true;
        }
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphExternalComponent constructLink :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.loopFlag::', this.loopFlag,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }
}

