import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef,
    HostListener,
    Input,
    OnInit,
    ViewChild
} from '@angular/core';
import { TreeGraphForceDirected } from '../../../../../models/tree-graph-force-directed.model';
import { GraphNode } from '../../../../../models/graph-node.model';
import { GraphLinkPath } from '../../../../../models/graph-link-path.model';
import { D3Service } from '../../../../../../../services/d3/d3.service';
import { DynamicComponentLoaderComponent } from '../../../../shared/dynamic-component-loader/dynamic-component-loader.component';
import { DynamicComponentResolverService } from '../../../../../../../services/dynamic-component-resolver.service';
import { DynamicComponentType } from '../../../../shared/dynamic-component-loader/dynamic-component-type';
import { Power2, TweenMax } from 'gsap/all';

@Component({
    // selector: 'svg[app-tree-graph], app-tree-graph',
    selector: 'app-tree-graph',
    templateUrl: './tree-graph.component.html',
    styleUrls: ['./tree-graph.component.scss']
})
export class TreeGraphComponent implements OnInit, AfterViewInit {
    @Input('nodes') public nodes: GraphNode[];
    @Input('links') public links: GraphLinkPath[];
    public currentNodes: any;
    public dynamicComponents: DynamicComponentType;
    // public dynamicComponents: Array<object>;
    public treeGraphForceDirected: TreeGraphForceDirected;
    public windowOptions: { width, height } = {width: 800, height: 600};
    @ViewChild('svgReference', {static: true}) svgReference: ElementRef;
    @ViewChild('dynamicContainer', {static: true}) dynamicContainer: ElementRef;
    @ViewChild(DynamicComponentLoaderComponent, {static: false}) dynamicComponent: DynamicComponentLoaderComponent;

    @HostListener('window:resize', [])
    onResize() {
        this.treeGraphForceDirected.initSimulation(this.options);
    }

    constructor(/*gggggg()(|I|)()gggggg*/
                private d3Service: D3Service,
                private changeDetectorRef: ChangeDetectorRef,
                private dynamicComponentResolverService: DynamicComponentResolverService,
                /*gggggg()(|I|)()gggggg*/) {
    }

    ngOnInit() {
        this.currentNodes = this.nodes;
        /**
         * Receiving an initialized simulated treeGraphForceDirected from our custom d3 service
         **/
        this.treeGraphForceDirected = this.d3Service.getForceDirectedTreeGraph(this.nodes, this.links, this.options);

        /**
         * Binding change detection check on each tick
         * This along with an onPush change detection strategy should enforce checking only when relevant!
         * This improves scripting computation duration in a couple of tests I've made, consistently.
         * Also, it makes sense to avoid unnecessary checks when we are dealing only with simulations data binding.
         **/
        this.treeGraphForceDirected.ticker.subscribe((d) => {
            this.changeDetectorRef.markForCheck();
            /*            console.log(
                            '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent ngOnInit ticker :::::::::::::::::::::::::::::::::::::::::::::::::::',
                            '\n::this::', this,
                            '\n::d::', d,
                            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                        );*/
        });

        /*console.log(
            '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.treeGraphForceDirected::', this.treeGraphForceDirected,
            '\n::this.currentNodes::', this.currentNodes,
            '\n::this.nodes::', this.nodes,
            '\n::this.nodes.length::', this.nodes.length,
            '\n::this.links::', this.links,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/
    }

    ngAfterViewInit() {
        this.treeGraphForceDirected.initTreeSimulation(this.options);
        // this.componentFactoryResolver.resolveComponentFactory();
        // const factories = Array.from(this.componentFactoryResolver['_factories'].values());
        // const factoryClass = <Type<any>>factories.find((x: any) => x.componentType.name === 'SystemMessageComponent');
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent routeUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::factories::', factories,
        //     '\n::factoryClass::', factoryClass,
        //     '\n::this.componentFactoryResolver::', this.componentFactoryResolver,
        //     '\n::this.componentFactoryResolver[\'_factories\'::', this.componentFactoryResolver['_factories'],
        //     '\n::this.componentFactoryResolver[\'_factories\'].values()::', this.componentFactoryResolver['_factories'].values(),
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get options() {
        return this.windowOptions = {
            width: window.innerWidth,
            height: window.innerHeight
        };
    }

    /*    public routeUpdate() {

            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent routeUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::this::', this,
                // '\n::this.node::', this.node,
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );
        }*/

    public eventManager(index: number, $event: MouseEvent) {

        /*        console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent eventManager :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::$event::', $event,
                    '\n::index::', index,
                    '\n::this.nodes[index]::', this.nodes[index],
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );*/
        if ( index !== null &&  typeof this.nodes[index].displayTypes !== 'undefined') {
            this.getDynamicComponent(index);
            this.containerBuild($event);
        } else if ( index === null ) {
            this.dynamicComponents = undefined;
            this.containerUnbuild($event);
        }
    }

    private getDynamicComponent(index: number) {
        if ( typeof this.nodes[index].displayTypes !== 'undefined' ) {
            this.dynamicComponents = this.dynamicComponentResolverService.getDynamicComponents(this.nodes[index].displayTypes[0]);
            /*            console.log(
                            '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent getDynamicComponent :::::::::::::::::::::::::::::::::::::::::::::::::::',
                            '\n::this::', this,
                            '\n::$event::', $event,
                            // '\n::testObject::', testObject,
                            '\n::this.nodes[index].displayTypes[0]::', this.nodes[index].displayTypes[0],
                            // '\n::typeof this.nodes[index].displayTypes[0]::', typeof this.nodes[index].displayTypes[0],
                            // '\n::index::', index,
                            // '\n::this.nodes[index].displayTypes.length > 0::', this.nodes[index].displayTypes.length > 0,
                            // '\n::this.nodes::', this.nodes,
                            // '\n::this.nodes[index]::', this.nodes[index],
                            // '\n::this.nodes[index].displayTypes::', this.nodes[index].displayTypes,
                            // '\n::this.nodes[index].displayTypes::', this.nodes[index].displayTypes.length,
                            // '\n::typeof testObject::', typeof testObject,
                            // '\n::this.nodes[index].displayTypes[0][\'type\']::', this.nodes[index].displayTypes[0]['type'],
                            // '\n::this.nodes[index].displayTypes[0][\'data\']::', this.nodes[index].displayTypes[0]['data'],
                            '\n::this.dynamicComponents::', this.dynamicComponents,
                            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                        );*/
        }
    }

    private getOffset(element) {
        const bound = element.getBoundingClientRect();
        const html = document.documentElement;

        return {
            top: bound.top + window.pageYOffset - html.clientTop,
            left: bound.left + window.pageXOffset - html.clientLeft
        };
    }

    private containerBuild($event: any) {
        let el = $event.target;
        let currentTarget: any = this.svgReference.nativeElement.getElementById($event.target.parentElement.id);
        let activeElement: any = this.getOffset($event.target);
        /*        console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent containerBuild :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::currentTarget::', currentTarget,
                    // '\n::this.dynamicContainer::', this.dynamicContainer,
                    // '\n::this.dynamicContainer[\'componentHost\'].viewContainerRef.element.nativeElement::', this.dynamicContainer['componentHost'].viewContainerRef.element.nativeElement,
                    // '\n::this.dynamicContainer[\'componentHost\'].viewContainerRef.element::', this.dynamicContainer['componentHost'].viewContainerRef.element,
                    // '\n::this.dynamicContainer.nativeElement::', this.dynamicContainer,
                    // '\n::this.dynamicContainer::', this.dynamicContainer,
                    '\n::this.getOffset(currentTarget)::', this.getOffset(currentTarget),
                    '\n::this.getOffset($event.target::', this.getOffset($event.target),
                    '\n::$event::', $event,
                    '\n::$event...matrix.e::', $event.target.parentNode.transform.animVal[0].matrix.e,
                    '\n::$event...matrix.f::', $event.target.parentNode.transform.animVal[0].matrix.f,
                    '\n::$event.target.parentElement.id::', $event.target.parentElement.id,
                    // '\n::$event.x::', $event.x,
                    // '\n::$event.y::', $event.y,
                    // '\n::$event.target[\'attributes\']::', $event.target['attributes'],
                    // '\n::$event.target[\'attributes\']::', $event.target.clientX,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );*/
        TweenMax.set(this.dynamicContainer.nativeElement, {
            top: '0',
            left: '0',
            autoAlpha: 0.01,
            // width: '100vw',
            // height: '100vh',
            width: '100%',
            height: '100%',
        });
        TweenMax.set(this.dynamicComponent.dynamicComponentLoader.nativeElement, {
            // top: (activeElement.y) + 'px',
            // left: (activeElement.x) + 'px',
            top: this.getOffset($event.target).top,
            left: this.getOffset($event.target).left,
            // top: $event.screenY,
            // left: $event.screenX,
            // top: $event.target.parentNode.transform.animVal[0].matrix.f,
            // left: $event.target.parentNode.transform.animVal[0].matrix.e,
            width: '100px',
            height: '100px',
            borderRadius: '100%',
            scale: 0.10,
            autoAlpha: 0.1,
            onCompleteScope: this,
            onComplete: this.build,
        });
    }

    private build() {
        TweenMax.to(this.dynamicContainer.nativeElement, 0.25, {
            autoAlpha: 0.99,
            ease: Power2.easeStraight,
        });
        TweenMax.to(this.dynamicComponent.dynamicComponentLoader.nativeElement, 0.55, {
            // top: '25vh',
            // left: '25vw',
            // height: '50vh',
            // width: '50vw',
            top: '25%',
            left: '25%',
            // height: '50%',
            // width: '50%',
            height: '50vw',
            width: '50vw',
            // height: '300px',
            // width: '300px',
            scale: 1.0,
            overflowY: 'hidden',
            autoAlpha: 0.25,
            backgroundColor: 'rgba(255, 255, 255, 0.9)',
            ease: Power2.easeIn,
            onCompleteScope: this,
            onComplete: this.build2,
        });
    }

    private build2() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent build2 :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        TweenMax.to(this.dynamicComponent.dynamicComponentLoader.nativeElement, 0.25, {
            top: '65px',
            left: '0px',
            borderRadius: '0%',
            autoAlpha: 0.99,
            height: '100%',
            width: '100%',
            // overflowY: 'auto',
            ease: Power2.easeOut,
            onCompleteScope: this,
            onComplete: this.buildComplete,
        });
    }

    private buildComplete() {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent buildComplete :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        TweenMax.set(this.dynamicComponent.dynamicComponentLoader.nativeElement, {
            overflowY: 'auto'
        });
    }

    private containerUnbuild($event: any) {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent containerUnbuild :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        TweenMax.to(this.dynamicContainer.nativeElement, 0.85, {
            // top: '50vh',
            // left: '50vw',
            // height: '0',
            // width: '0',
            autoAlpha: 0.1,
            onCompleteScope: this,
            onComplete: this.buildReset,
        });
        TweenMax.to(this.dynamicComponent.dynamicComponentLoader.nativeElement, 0.85, {
            // scale: 0.01,
            // height: '10px',
            // width: '10px',
            autoAlpha: 0.01,
            // borderRadius: '100%',
            ease: Power2.easeOut,
        });
    }

    private buildReset() {

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: TreeGraphComponent buildReset :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        TweenMax.set(this.dynamicContainer.nativeElement, {
            top: '0',
            left: '0',
            width: '0',
            height: '0',
        });
    }
}
