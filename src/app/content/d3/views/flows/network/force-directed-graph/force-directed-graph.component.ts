import { Component, OnInit } from '@angular/core';
import { GraphNode } from '../../../../models/graph-node.model';
import { GraphLink } from '../../../../models/graph-link.model';
// import { ForceDirectedService } from '../../../../../../services/force-directed.service';

import CONFIG from './force-directed-graph.configuration';

@Component({
    selector: 'app-force-directed-graph',
    templateUrl: './force-directed-graph.component.html',
    styleUrls: ['./force-directed-graph.component.scss']
})
export class ForceDirectedGraphComponent implements OnInit {
    nodes: GraphNode[] = [];
    links: GraphLink[] = [];
    public loopFlag: boolean;

    constructor(/*SSSŠSSSSSS|Š|SSSSSSŠSSS*/
                // private forceDirectedService: ForceDirectedService,
                /*SSSŠSSSSSS|Š|SSSSSSŠSSS*/) {
        // const N = forceDirectedGraph.CONFIG.N;
        const configEnumeration = CONFIG.N;
        const getIndex = nodeNumber => nodeNumber - 1;

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphComponent constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.nodes::', this.nodes,
            '\n::this.links::', this.links,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        /** constructing the nodes array */
        for (let i = 1; i <= configEnumeration; i++) {
            this.nodes.push(new GraphNode(i));
            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphComponent for1 :::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::configEnumeration::', configEnumeration,
                '\n::i ::', i,
                '\n::this.nodes ::', this.nodes,
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );
        }

        for (let i = 1; i <= configEnumeration; i++) {
            for (let m = 2; i * m <= configEnumeration; m++) {
                console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphComponent for3 :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::configEnumeration::', configEnumeration,
                    '\n::i * m::', i * m,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
                /** increasing connections toll on connecting nodes */
                this.nodes[getIndex(i)].linkCount++;
                this.nodes[getIndex(i * m)].linkCount++;

                /** connecting the nodes before starting the simulation */
                this.links.push(new GraphLink(i, i * m));
            }
            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphComponent for2 :::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::configEnumeration::', configEnumeration,
                '\n::i ::', i,
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );
            if ( configEnumeration === i ) {
                this.loopFlag = true;
            } else {
                this.loopFlag = false;
            }
        }
    }

    ngOnInit(): void {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraphComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.nodes::', this.nodes,
        //     '\n::this.links::', this.links,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }
}
