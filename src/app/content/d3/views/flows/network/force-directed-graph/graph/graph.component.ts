import { AfterViewInit, ChangeDetectorRef, Component, Input, HostListener, OnInit } from '@angular/core';
import { D3Service } from '../../../../../../../services/d3/d3.service';
import { GraphForceDirected } from '../../../../../models/graph-force-directed.model';

@Component({
    selector: 'app-graph',
    templateUrl: './graph.component.html',
    styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit, AfterViewInit {

    @Input() public nodes;
    @Input() public links;
    public currentNodes:any;
    public graphForceDirected: GraphForceDirected;
    public windowOptions: { width, height } = {width: 800, height: 600};

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.graphForceDirected.initSimulation(this.options);
    }

    constructor(/*gggggg()(|I|)()gggggg*/
                private d3Service: D3Service,
                private changeDetectorRef: ChangeDetectorRef
                /*gggggg()(|I|)()gggggg*/) {
    }

    ngOnInit() {
        this.currentNodes = this.nodes;
        /** Receiving an initialized simulated graphForceDirected from our custom d3 service */
        this.graphForceDirected = this.d3Service.getForceDirectedGraph(this.nodes, this.links, this.options);

        /** Binding change detection check on each tick
         * This along with an onPush change detection strategy should enforce checking only when relevant!
         * This improves scripting computation duration in a couple of tests I've made, consistently.
         * Also, it makes sense to avoid unnecessary checks when we are dealing only with simulations data binding.
         */
        this.graphForceDirected.ticker.subscribe((d) => {
            this.changeDetectorRef.markForCheck();
        });

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: GraphComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.graphForceDirected::', this.graphForceDirected,
            '\n::this.currentNodes::', this.currentNodes,
            '\n::this.nodes::', this.nodes,
            '\n::this.nodes.length::', this.nodes.length,
            '\n::this.links::', this.links,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

    ngAfterViewInit() {
        this.graphForceDirected.initSimulation(this.options);
    }

    get options() {
        return this.windowOptions = {
            width: window.innerWidth,
            height: window.innerHeight
        };
    }
}
