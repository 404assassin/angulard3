import { AfterViewInit, ChangeDetectorRef, Component, HostListener, Input, OnInit } from '@angular/core';
import { D3Service } from '../../../../../../../services/d3/d3.service';
import { MenuService } from '../../../../../../../services/menu.service';
import { GraphForceDirected } from '../../../../../models/graph-force-directed.model';

@Component({
    selector: 'app-hierarchy-tree',
    templateUrl: './hierarchy-tree.component.html',
    styleUrls: ['./hierarchy-tree.component.scss']
})
export class HierarchyTreeComponent implements OnInit, AfterViewInit {
    @Input() public hierarchyData;
    @Input() public nodes;
    @Input() public links;
    public graphForceDirected: GraphForceDirected;

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        // this.graphForceDirected.initSimulation(this.options);
    }

    public windowOptions: { width, height } = {width: 2000, height: 1800};
    public viewBoxParameters: any = '0 0 ' + this.windowOptions.width + ' ' + this.windowOptions.height;

    constructor(/*====[||}=+={||]====++====[||}=+={||]====*/
                private d3Service: D3Service,
                private changeDetectorRef: ChangeDetectorRef,
                private menuService: MenuService,
                /*====[||}=+={||]====++====[||}=+={||]====*/) {
    }

    ngOnInit() {
        /** Receiving an initialized simulated graphForceDirected from our custom d3 service */
        // this.graphForceDirected = this.d3Service.getForceDirectedGraph(this.nodes, this.links, this.options);

        /** Binding change detection check on each tick
         * This along with an onPush change detection strategy should enforce checking only when relevant!
         * This improves scripting computation duration in a couple of tests I've made, consistently.
         * Also, it makes sense to avoid unnecessary checks when we are dealing only with simulations data binding.
         */
        // this.graphForceDirected.ticker.subscribe((d) => {
        //     this.changeDetectorRef.markForCheck();
        // });

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: HierarchyTreeComponent ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.hierarchyData::', this.hierarchyData,
            // '\n::this.graphForceDirected::', this.graphForceDirected,
            // '\n::this.nodes::', this.nodes,
            // '\n::this.links::', this.links,
            // '\n::this.links::', this.links,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

    ngAfterViewInit() {
        // this.graphForceDirected.initSimulation(this.options);
    }

    public get options() {
        return this.windowOptions = {
            width: window.innerWidth,
            height: window.innerHeight
        };
    }

}
