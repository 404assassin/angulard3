import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../../../../../services/menu.service';
import { FlattenDataService } from '../../../../../../services/flatten-data.service';

@Component({
    selector: 'app-force-directed-tree',
    templateUrl: './force-directed-tree.component.html',
    styleUrls: ['./force-directed-tree.component.scss']
})
export class ForceDirectedTreeComponent implements OnInit {
    public hierarchyData: any;
    public flatData: any;

    constructor(/*====[||}=+={||]====++====[||}=+={||]====*/
                private flattenDataService: FlattenDataService,
                private menuService: MenuService,
                /*====[||}=+={||]====++====[||}=+={||]====*/) {
        this.getHierarchyData();

    }

    ngOnInit() {
    }

    private getHierarchyData() {
        this.menuService.getDatas().subscribe(
            (data) => {
                this.hierarchyData = data;
                console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedTreeComponent getHierarchyData :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::this.hierarchyData::', this.hierarchyData,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            (error) => {
                console.error(
                    '\n::::::::::::::::::::::::::::::::::::::  ForceDirectedTreeComponent  GET  Error  ::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::POST Request Error ::', error,
                    '\n::POST Request Error Message ::', error.message,
                    '\n::POST Request Error Message ::', error.error.message,
                    '\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            },
            () => this.dataUpdate()
        );
    }

    private dataUpdate() {
        this.flatData = this.flattenDataService.getFlatted(this.hierarchyData);

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedTreeComponent dataUpdate :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.flatData::', this.flatData,
            '\n::this.hierarchyData::', this.hierarchyData,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }
}
