import CONFIG from '../views/flows/network/force-directed-graph/force-directed-graph.configuration';
import * as d3 from 'd3';

export class GraphNode implements d3.SimulationNodeDatum {
    // optional - defining optional implementation properties - required for relevant typing assistance
    private _color?: string;
    private _displayTypes?: Array<object>;
    private _labelColor?: string;
    private _fontSize?: string;
    private _label?: string;
    private _radius?: number;
    private _route?: string;
    private _stroke?: string;
    private _strokeWidth?: number;
    index?: number;
    x?: number;
    y?: number;
    vx?: number;
    vy?: number;
    fx?: number | null;
    fy?: number | null;

    id: string;
    linkCount: any = 0;

    constructor(id) {
        this.id = id;
    }

    private normal = () => {
        // return Math.sqrt(this.linkCount / CONFIG.N);
        return Math.sqrt(this.linkCount / 10);
        // return Math.sqrt(this.linkCount / 150);
    };

    get r() {
        if ( typeof this._radius === 'undefined' ) {
            return 50 * this.normal() + 20;
        } else {
            return this._radius;
        }
    }

    set r(r: number) {
        this._radius = r;
    }

    set fontSize(fontSize) {
        this._fontSize = fontSize;
    }

    get fontSize() {
        if ( typeof this._fontSize === 'undefined' ) {
            return (30 * this.normal() + 10) + 'px';
        } else {
            return this._fontSize;
        }
    }

    get color(): string {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model getColor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._color::', this._color,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._color === 'undefined' ) {
            const index = Math.floor(CONFIG.SPECTRUM.length * this.normal());
            return CONFIG.SPECTRUM[index];
        } else {
            return this._color;
        }
    }

    set color(color) {
        this._color = color;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model setColor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._color::', this._color,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get labelColor(): string {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model getColor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._labelColor::', this._labelColor,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._labelColor === 'undefined' ) {
            const index = Math.floor(CONFIG.SPECTRUM.length * this.normal());
            return CONFIG.SPECTRUM[index];
        } else {
            return this._labelColor;
        }
    }

    set labelColor(labelColor) {
        this._labelColor = labelColor;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model setColor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._labelColor::', this._labelColor,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get stroke(): string {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model getStrokeColor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._stroke::', this._stroke,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._stroke !== 'undefined' ) {
            return this._stroke;
        }
    }

    set stroke(stroke) {
        this._stroke = stroke;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model setStrokeColor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._stroke::', this._stroke,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get strokeWidth(): number {
        if ( typeof this._strokeWidth !== 'undefined' ) {
            return this._strokeWidth;
        }
    }

    set strokeWidth(strokeWidth) {
        this._strokeWidth = strokeWidth;
    }

    get label(): string {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model get label :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._color::', this._color,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._label !== 'undefined' ) {
            return this._label;
        }
    }

    set label(label) {
        this._label = label;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model set label :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._color::', this._color,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get route(): string {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model get route :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._color::', this._color,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._route !== 'undefined' ) {
            return this._route;
        }
    }

    set route(route) {
        this._route = route;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model set route :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._color::', this._color,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get displayTypes(): Array<object> {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model get displayTypes :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._color::', this._color,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._displayTypes !== 'undefined' ) {
            return this._displayTypes;
        }
    }

    set displayTypes(displayTypes) {
        this._displayTypes = displayTypes;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphNode model set displayTypes :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._color::', this._color,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }
}
