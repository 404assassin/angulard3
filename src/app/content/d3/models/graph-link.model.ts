import { GraphNode } from './graph-node.model';
import * as d3 from 'd3';

export class GraphLink implements d3.SimulationLinkDatum<GraphLink> {
    // Optional - defining optional implementation properties - required for relevant typing assistance
    private _stroke: string;
    private _strokeWidth: number;
    private _strokeDashArray: Array<number>;
    public index?: number;

    // Must - defining enforced implementation properties
    public source: GraphLink | string | number;
    public target: GraphLink | string | number;

    constructor(source, target) {
        this.source = source;
        this.target = target;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLink constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::source::', source,
        //     '\n::target::', target,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get strokeDashArray(): Array<number> {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLink model get strokeDashArray :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._strokeDashArray::', this._strokeDashArray,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._strokeDashArray !== 'undefined' ) {
            return this._strokeDashArray;
        }
    }

    set strokeDashArray(strokeDashArray) {
        this._strokeDashArray = strokeDashArray;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLink model set strokeDashArray :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._strokeDashArray::', this._strokeDashArray,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get strokeWidth(): number {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLink model get strokeWidth :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._strokeWidth::', this._strokeWidth,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._strokeWidth !== 'undefined' ) {
            return this._strokeWidth;
        }
    }

    set strokeWidth(strokeWidth) {
        this._strokeWidth = strokeWidth;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLink model set strokeWidth :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._strokeWidth::', this._strokeWidth,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    get stroke(): string {
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLink model get stroke :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._stroke::', this._stroke,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        if ( typeof this._stroke !== 'undefined' ) {
            return this._stroke;
        }
    }

    set stroke(stroke) {
        this._stroke = stroke;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GraphLink model set stroke :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this._stroke::', this._stroke,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }
}
