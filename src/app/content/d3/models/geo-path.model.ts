import * as d3 from 'd3';

export class GeoPath implements d3.SimulationNodeDatum {
    index?: number;
    pathData?: any;

    private path = () => {
        // return Math.sqrt(this.linkCount / CONFIG.N);
        // return this.linkCount;
    };

    get m() {
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: GeoPath Model m :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.pathData::', this.pathData,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        return this.pathData;
    }
}






