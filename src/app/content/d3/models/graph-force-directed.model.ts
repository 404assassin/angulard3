import { EventEmitter } from '@angular/core';
import { GraphLink } from './graph-link.model';
import { GraphNode } from './graph-node.model';
import * as d3 from 'd3';

const FORCES = {
    LINKS: 1 / 50,
    COLLISION: 1,
    CHARGE: -1
};

export class GraphForceDirected {
    public ticker: EventEmitter<d3.Simulation<GraphNode, GraphLink>> = new EventEmitter();
    public simulation: d3.Simulation<any, any>;

    constructor(/*[][[][][][][][][][][]][]*/
                public graphNode: GraphNode[],
                private graphLink: GraphLink[],
                options: { width, height },
                /*[][[][][][][][][][][]][]*/) {
        this.graphNode = graphNode;
        this.graphLink = graphLink;

        /*console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ForceDirectedGraph constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.graphNode::', this.graphNode,
            '\n::this.graphLink::', this.graphLink,
            '\n::options: { width, height }::', options.width,
            '\n::options: { width, height }::', options.height,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );*/
        this.initSimulation(options);
    }

    connectGraphNodes(source: any, target: any): void {
        let link;

        if ( !this.graphNode[source] || !this.graphNode[target] ) {
            throw new Error('One of the graphNode does not exist');
        }

        link = new GraphLink(source, target);
        this.simulation.stop();
        this.graphLink.push(link);
        this.simulation.alphaTarget(0.3).restart();

        this.initGraphLinks();
    }

    initGraphNodes(): void {
        if ( !this.simulation ) {
            throw new Error('simulation was not initialized yet');
        }
        this.simulation.nodes(this.graphNode);
    }

    initGraphLinks(): void {
        if ( !this.simulation ) {
            throw new Error('simulation was not initialized yet');
        }

        this.simulation.force('graphLink',
            d3.forceLink(this.graphLink)
                .id(d => d.id)
                .strength(FORCES.LINKS)
        );
    }

    initSimulation(options): void {
        if ( !options || !options.width || !options.height ) {
            throw new Error('missing options when initializing simulation');
        }

        /** Creating the simulation */
        if ( !this.simulation ) {
            const ticker = this.ticker;

            this.simulation = d3.forceSimulation()
                .force('charge',
                    d3.forceManyBody()
                        .strength(d => FORCES.CHARGE * d.r)
                )
                .force('collide',
                    d3.forceCollide()
                        .strength(FORCES.COLLISION)
                        .radius(d => d.r + 5).iterations(2)
                );

            // Connecting the d3 ticker to an angular event emitter
            this.simulation.on('tick', () => {
                ticker.emit(this);
            });

            this.initGraphNodes();
            this.initGraphLinks();
        }

        /** Updating the central force of the simulation */
        this.simulation.force('centers', d3.forceCenter(options.width / 2, options.height / 2));

        /** Restarting the simulation internal timer */
        this.simulation.restart();
    }
}
