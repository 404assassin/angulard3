import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content.component';
import { ForceDirectedGraphComponent } from './d3/views/flows/network/force-directed-graph/force-directed-graph.component';
import { ForceDirectedGraphExternalComponent } from './d3/views/flows/network/force-directed-graph-external/force-directed-graph-external.component';
import { UsZoomableComponent } from './d3/views/maps/geo/us-zoomable/us-zoomable.component';
import { UsaStaticComponent } from './d3/views/maps/geo/usa-static/usa-static.component';
import { ForceDirectedTreeComponent } from './d3/views/flows/network/force-directed-tree/force-directed-tree.component';

const contentRoutes: Routes = [
    {
        path: 'types',
        component: ContentComponent,
        children: [
            {
                path: 'force-directed-graph-generated', component: ForceDirectedGraphComponent
            },
            {
                path: 'force-directed-graph-external', component: ForceDirectedGraphExternalComponent
            },
            {
                path: 'force-directed-tree', component: ForceDirectedTreeComponent
            },
            {
                path: 'us-zoomable', component: UsZoomableComponent
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(contentRoutes)],
    exports: [RouterModule]
})
export class ContentRoutingModule {
}
