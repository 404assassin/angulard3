import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MenuService {
    private dataSubject: BehaviorSubject<object[]> = new BehaviorSubject([]);
    public data$: Observable<object[]> = this.dataSubject.asObservable();

    /**
     * Creates a new API Get Service via environment specified local.
     * @param {Http} http - The injected Http.
     * @constructor
     */
    constructor(/*X*/
                public http: HttpClient
                /*X*/) {
    }

    public getDatas(): any {
        // console.log(
        //     '\n::::::::::::::::::::::::::::::::::::::  MenuService  getDatas  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return this.http.get(environment.localData + 'menu.json', {responseType: 'json'});
    }

    public updateData(): Observable<any> {
        return this.getDatas().do((data) => {
            this.dataSubject.next(data);
        });
    }

}
