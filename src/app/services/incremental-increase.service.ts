import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class IncrementalIncreaseService {
    private up: boolean;
    private value: number;
    private increment: number;
    private ceiling: number;

    constructor() {
    }

    public incrementalIncrease(value: number, increment: number, ceiling: number): number {
        this.value = value;
        this.increment = increment;
        this.ceiling = ceiling;
        return this.performIncreaseCalculation();
    }

    private performIncreaseCalculation() {
        if ( this.up === true && this.value <= this.ceiling ) {
            this.value += this.increment;
            if ( this.value === this.ceiling ) {
                this.up = false;
            }
        } else {
            this.up = false;
            this.value -= this.increment;
            if ( this.value === 0 ) {
                this.up = true;
            }
        }
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: IncrementalIncreaseService performIncreaseCalculation :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::this.value::', this.value,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        return this.value;
    }
}
