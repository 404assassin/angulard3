import { TestBed } from '@angular/core/testing';

import { GetElementAngleService } from './get-element-angle.service';

describe('GetElementAngleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetElementAngleService = TestBed.get(GetElementAngleService);
    expect(service).toBeTruthy();
  });
});
