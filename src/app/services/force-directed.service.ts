import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ForceDirectedService {
    private dataSubject: BehaviorSubject<object[]> = new BehaviorSubject([]);
    public data$: Observable<object[]> = this.dataSubject.asObservable();

    /**
     * Creates a new API Get Service via environment specified local.
     * @param {Http} http - The injected Http.
     * @constructor
     */
    constructor(/*X*/
                public http: HttpClient
                /*X*/) {
    }

    public getDatas(): any {
        console.log(
            '\n::::::::::::::::::::::::::::::::::::::  ForceDirectedService  getDatas  :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::environment.localData + \'force-directed.json\'::', environment.localData + 'force-directed.json',
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        return this.http.get(environment.localData + 'force-directed.json', {responseType: 'json'});
    }

}

