import { ElementRef, Injectable } from '@angular/core';
import { BoundingBoxOffsetService } from '../bounding-box-offset.service';
import { DraggableService } from '../gsap/draggable.service';

import Draggable from "gsap/Draggable";
import * as d3 from 'd3';
import * as topographyJsonClient from 'topojson-client';

import { Bounce, Power2, TweenMax } from 'gsap/all';

@Injectable({
    providedIn: 'root'
})
export class GeoService {
    public projectionGeoAldersUSA: any;

    constructor(/*::::::[[[{|||}]]]::::::*/
                private boundingBoxOffsetService: BoundingBoxOffsetService,
                private draggableService: DraggableService,
                // private elementRef: ElementRef,
                /*::::::[[[{|||}]]]::::::*/) {
    }

    /**
     * Map Projections
     */
    /**
     * Construct GEO Path
     */
    public getGeoPath(projection: any): any {
        const path: any = d3.geoPath(projection);
        /*        console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: GeoService getGeoPath :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::projection::', projection,
                    // '\n::geoGeometryData::', geoGeometryData,
                    '\n::path::', path,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );*/
        // return path(geoGeometryData);
        return path;
    }

    /**
     * GEO data for USA
     */
    public getProjectionGeoAldersUSA(geoScale: any, translateParams: any): any {
        const projection = d3.geoAlbersUsa()
            .precision([0, 0])
            .translate([translateParams.width / 2, translateParams.height / 2])
            .scale(geoScale);
        // this.projectionGeoAldersUSA = projection;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GeoService getProjectionGeoAldersUSA :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::translateParams::', translateParams,
        //     '\n::geoScale::', geoScale,
        //     '\n::projection::', projection,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        return projection;
    }

    /**
     * GEO data for USA
     */
    public getGeoAldersUSA(geoData: any, geoScale: any, translateParams: any): any {
        const projection = this.getProjectionGeoAldersUSA(geoScale, translateParams);
        const stateIdAccessor = geoData.id;
        const stateGeometryAccessor = geoData.geometry;
        const statePropertiesAccessor = geoData.properties;
        const stateTypeAccessor = geoData.type;
        const path = this.getGeoPath(projection);
        /*        console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: GeoService getGeoAldersUSA :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    // '\n::geoData::', geoData,
                    // '\n::stateIdAccessor::', stateIdAccessor,
                    // '\n::geoData.geometry::', geoData.geometry,
                    // '\n::geoScale::', geoScale,
                    '\n::stateGeometryAccessor::', stateGeometryAccessor,
                    // '\n::statePropertiesAccessor::', statePropertiesAccessor,
                    // '\n::stateTypeAccessor::', stateTypeAccessor,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );*/
        return path(stateGeometryAccessor);
    }

    /**
     * A method to bind a pan and zoom behaviour to an svg element
     */
    public zoomToBoundingBox(elementContainer: any, svgPathElement: any, svgPathParent: any, path: any, pathData: any, translateParams: any): void {

        let bounds = path.bounds(pathData),
            dx = bounds[1][0] - bounds[0][0],
            dy = bounds[1][1] - bounds[0][1],
            x = (bounds[0][0] + bounds[1][0]) / 2,
            y = (bounds[0][1] + bounds[1][1]) / 2,
            scale = Math.max(1, Math.min(8, 0.8 / Math.max(dx / translateParams.width, dy / translateParams.height))),
            translate = [translateParams.width / 2 - scale * x, (translateParams.height) / 2 - scale * y];

        this.setPathStroke(elementContainer, svgPathElement, svgPathParent, scale);
        TweenMax.set(svgPathElement, {
            autoAlpha: 0.01,
        });

        TweenMax.to(elementContainer.getElementById('statesSvg'), 0.75, {
            scale: scale,
            autoAlpha: 1.0,
            // dx: dx,
            // dy: dy,
            x: translate[0],
            y: translate[1],
            transformOrigin: "-8%" + " " + "-5%",
            ease: Power2.easeOut,
            onCompleteScope: this,
            onComplete: this.draggableService.makeDraggable(elementContainer.getElementById('statesSvg'))
        });

        // this.draggableOn(elementContainer, elementContainer.getElementById('statesSvg'));
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GeoService zoomToBoundingBox :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::svgPathElement::', svgPathElement,
        //     '\n::svgPathElement::', typeof svgPathElement,
        //     '\n::scale::', scale,
        //     '\n::dx::', dx,
        //     '\n::dy::', dy,
        //     '\n::bounds::', bounds,
        //     '\n::translateParams.width::', translateParams.width,
        //     '\n::x::', x,
        //     '\n::bounds::', bounds,
        //     '\n::translate[0]::', translate[0],
        //     '\n::translate[1]::', translate[1],
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    /**
     * A method to set the Path Stroke of an svg element
     */
    public setPathStroke(elementContainer: any, svgPathElement: any, svgPathParent: any, scale: any): void {
        let pathParent: any = d3.select(svgPathParent);
        let strokeAmount: any = Math.ceil(0.075 / scale * 100) / 100 + '%';

        pathParent.raise();

        TweenMax.to(elementContainer.getElementsByTagName('path'), 0.75, {
            // stroke: 'rgba(116,3,5,1.0)',
            stroke: 'rgb(255, 255, 255, 1.0)',
            strokeWidth: strokeAmount,
        });

        TweenMax.to(svgPathElement, 0.75, {
            stroke: 'rgba(8,101,209,1.0)',
            autoAlpha: 0.9,
        });
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GeoService zoomToBoundingBox :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::svgPathElement::', svgPathElement,
        //     '\n::pathParent::', pathParent,
        //     '\n::svgPathElement::', typeof svgPathElement,
        //     '\n::scale::', scale,
        //     '\n::strokeAmount::', strokeAmount,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    /**
     * A method to bind a pan and zoom behaviour to an svg element
     */
    zoom($event: any): void {
        // let zoomed: any;
        // d3.zoom().on('zoom', () => {
        //     console.log(
        //         '\n:::::::::::::::::::::::::::::::::::::: GeoService zoomToBoundingBox zoom :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //         '\n::this::', this,
        //         '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        //     );
        //     zoomed.emit(this);
        // });

        // TweenMax.to(this.portfolioNavigationElement.nativeElement, 0.5, {
        //     top: 0,
        //     svgOrigin:'100 100',
        //     autoAlpha: 1,
        //     ease: Power2.easeOut
        // });
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GeoService zoom :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::d3::', d3,
        //     '\n::$event::', $event,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    /**
     * A method to rest an svg path elements path
     */
    resetPathStroke(elementContainer: any,): void {

        TweenMax.to(elementContainer.getElementsByTagName('path'), 0.75, {
            // stroke: 'rgba(116,3,5,1.0)',
            stroke: 'rgb(255, 255, 255, 1.0)',
            strokeWidth: '0.025%',
            autoAlpha: 1.0,
        });

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GeoService resetBoundingBoxZoom :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::elementContainer::', elementContainer,
        //     '\n::elementContainer.getElementById(\'statesSvg\')::', elementContainer.getElementById('statesSvg'),
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    /**
     * A method to bind a pan and zoom behaviour to an svg element
     */
    resetBoundingBoxZoom($eventTarget: any, elementContainer: any,): void {

        this.resetPathStroke(elementContainer);
        TweenMax.to(elementContainer.getElementById('statesSvg'), 0.75, {
            scale: 1,
            autoAlpha: 1.0,
            // dx: dx,
            // dy: dy,
            x: 0,
            y: 0,
            transformOrigin: "-8%" + " " + "-5%",
            ease: Power2.easeOut
        });

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GeoService resetBoundingBoxZoom :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$eventTarget::', $eventTarget,
        //     '\n::elementContainer::', elementContainer,
        //     '\n::elementContainer.getElementById(\'statesSvg\')::', elementContainer.getElementById('statesSvg'),
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    /**
     * A method to add zoom by fixed percentage behaviour to an svg element
     */
    zoomPercentage(elementContainer: any, zoomableElement: any, zoomPercentage: any, zoomOffsetX: any, zoomOffsetY: any): void {
        TweenMax.to(elementContainer.getElementsByTagName('path'), 0.75, {
            // stroke: 'rgba(116,3,5,1.0)',
            stroke: 'rgb(255, 255, 255, 1.0)',
            strokeWidth: '0.025%',
            autoAlpha: 1.0,
        });

        TweenMax.to(elementContainer.getElementById('statesSvg'), 0.75, {
            scale: zoomPercentage,
            autoAlpha: 1.0,
            x: zoomOffsetX,
            y: zoomOffsetY,
            transformOrigin: "-8%" + " " + "-5%",
            ease: Power2.easeInOut,
            onCompleteScope: this,
            onComplete: this.draggableService.makeDraggable(zoomableElement)
        });

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: GeoService resetBoundingBoxZoom :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::elementContainer::', elementContainer,
        //     '\n::elementContainer.getElementById(\'statesSvg\')::', elementContainer.getElementById('statesSvg'),
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }
}
