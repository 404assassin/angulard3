import { Injectable } from '@angular/core';
import { GraphForceDirected } from '../../content/d3/models/graph-force-directed.model';
import { TreeGraphForceDirected } from '../../content/d3/models/tree-graph-force-directed.model';
import { GraphLink } from '../../content/d3/models/graph-link.model';
import { GraphLinkPath } from '../../content/d3/models/graph-link-path.model';
import { GraphNode } from '../../content/d3/models/graph-node.model';

import * as d3 from 'd3';
import * as topojson from "topojson-client";

/**
 * This service will provide methods to enable user interaction with elements while maintaining the d3 simulations physics
 */
@Injectable({
    providedIn: 'root'
})
export class D3Service {

    constructor(/*[[[]]][][][][][][][][][][][][][][][][][][][][][][][][][[[]]]*/
                // private graphForceDirected: GraphForceDirected
                /*[[[]]][][][][][][][][][][][][][][][][][][][][][][][][][[[]]]*/) {
    }

    /**
     * A method to bind a pan and zoom behaviour to an svg element
     */
    applyZoomableBehaviour(svgElement: any, containerElement: any): void {
        let svg: any;
        let container: any;
        let zoomed: any;
        let zoom: any;

        svg = d3.select(svgElement);
        container = d3.select(containerElement);

        zoomed = () => {
            const transform = d3.event.transform;
            /*console.log(
                '\n:::::::::::::::::::::::::::::::::::::: D3Service applyZoomableBehaviour zoomed :::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::this::', this,
                '\n::d3::', d3,
                '\n::d3.event::', d3.event,
                '\n::d3.event.transform::', d3.event.transform,
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );*/
            container.attr('transform', 'translate(' + transform.x + ',' + transform.y + ') scale(' + transform.k + ')');
        };

        zoom = d3.zoom().on('zoom', zoomed);
        svg.call(zoom);
    }

    /**
     * A method to bind a draggable behaviour to an svg element
     */
    applyDraggableBehaviour(element: any, graphNode: GraphNode, graphForceDirected: GraphForceDirected): void {
        const d3element = d3.select(element);

        const started = () => {
            /** Preventing propagation of dragstart to parent elements */
            d3.event.sourceEvent.stopPropagation();

            if ( !d3.event.active ) {
                graphForceDirected.simulation.alphaTarget(0.3).restart();
            }

            d3.event.on('drag', dragged).on('end', ended);
        };
        const dragged = () => {
            graphNode.fx = d3.event.x;
            graphNode.fy = d3.event.y;
        };
        const ended = () => {
            if ( !d3.event.active ) {
                graphForceDirected.simulation.alphaTarget(0);
            }

            graphNode.fx = null;
            graphNode.fy = null;
        };

        d3element.call(d3.drag()
            .on('start', started));
    }

    /**
     * This method does not interact with the document, purely physical calculations with d3
     */
    public getForceDirectedGraph(graphNodes: GraphNode[], graphLink: GraphLink[], options: { width, height }): GraphForceDirected {
        const graphForceDirected = new GraphForceDirected(graphNodes, graphLink, options);
        return graphForceDirected;
    }

    /**
     * A method to bind a draggable behaviour to an svg element
     */
    applyTreeDraggableBehaviour(element: any, graphNode: GraphNode, treeGraphForceDirected: TreeGraphForceDirected): void {
        const d3element = d3.select(element);

        const started = () => {
            /** Preventing propagation of dragstart to parent elements */
            d3.event.sourceEvent.stopPropagation();

            if ( !d3.event.active ) {
                treeGraphForceDirected.simulation.alphaTarget(0.3).restart();
            }

            d3.event.on('drag', dragged).on('end', ended);
        };
        const dragged = () => {
            graphNode.fx = d3.event.x;
            graphNode.fy = d3.event.y;
        };
        const ended = () => {
            if ( !d3.event.active ) {
                treeGraphForceDirected.simulation.alphaTarget(0);
            }

            graphNode.fx = null;
            graphNode.fy = null;
        };

        d3element.call(d3.drag()
            .on('start', started));
    }

    /**
     * This method does not interact with the document, purely physical calculations with d3
     */
    public getForceDirectedTreeGraph(graphNodes: GraphNode[], graphLinkPath: GraphLinkPath[], options: { width, height }): TreeGraphForceDirected {
        const treeGraphForceDirected = new TreeGraphForceDirected(graphNodes, graphLinkPath, options);
        return treeGraphForceDirected;
    }

}
