import { TestBed } from '@angular/core/testing';

import { DynamicHostStateService } from './dynamic-host-state.service';

describe('DynamicHostStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DynamicHostStateService = TestBed.get(DynamicHostStateService);
    expect(service).toBeTruthy();
  });
});
