import { TestBed } from '@angular/core/testing';

import { DynamicComponentResolverService } from './dynamic-component-resolver.service';

describe('DynamicComponentResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DynamicComponentResolverService = TestBed.get(DynamicComponentResolverService);
    expect(service).toBeTruthy();
  });
});
