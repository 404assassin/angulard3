import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
// import { Power2, Bounce, TweenMax } from 'gsap/all';
import Draggable from 'gsap/Draggable';
import ThrowPropsPlugin from '../../../assets/libraries/gsap-bonus/ThrowPropsPlugin';

const throwPropsPlugin: any = ThrowPropsPlugin;

@Injectable({
    providedIn: 'root'
})
export class DraggableService {
    private _onDragStart: Subject<boolean> = new Subject<boolean>();
    public _onDragStartObs = this._onDragStart.asObservable();
    private dragStarted: boolean;
    private globalInstance: any;

    constructor(/*=========*|*|{[o]}|*|*=========*/
                // private renderer2: Renderer2,
                // private throwPropsPlugin: ThrowPropsPlugin
                /*=========*|*|{[o]}|*|*=========*/) {
    }

    public makeDraggable(draggableElement: any) {
        Draggable.create(draggableElement, {
            edgeResistance: 0.65,
            type: "x,y",
            throwProps: false,
            throwResistance: 3000,
            onDragStartScope: this,
            onDragStart: ($event) => {
                // console.log(
                //     '\n:::::::::::::::::::::::::::::::::::::: DraggableService makeDraggable onDragStart :::::::::::::::::::::::::::::::::::::::::::::::::::',
                //     '\n::this::', this,
                //     '\n::$event::', $event,
                //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                // );
                this.onDragStart($event);
            },
            onDragEndScope: this,
            onDragEnd: ($event) => {
                // console.log(
                //     '\n:::::::::::::::::::::::::::::::::::::: DraggableService makeDraggable onDragEnd :::::::::::::::::::::::::::::::::::::::::::::::::::',
                //     '\n::this::', this,
                //     '\n::$event::', $event,
                //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                // );
                this.onDragEnd($event);
            },
        });
    }

    private onDragStart($event): void {
        this.dragStarted = true;
        this._onDragStart.next(this.dragStarted);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: DraggableService onDragStart :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$event::', $event,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    private onDragEnd($event): void {
        this.dragStarted = false;
        this._onDragStart.next(this.dragStarted);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: DraggableService onDragStart :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$event::', $event,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

    public makeDraggableWithBounds(dragContainer: any, draggableElement: any): void {
        Draggable.create(draggableElement, {
            bounds: dragContainer,
            edgeResistance: 0.65,
            type: "x,y",
            throwProps: true,
            onDragStartScope: this,
            onDragStart: ($event) => {
                // console.log(
                //     '\n:::::::::::::::::::::::::::::::::::::: DraggableService makeDraggable onDragStart :::::::::::::::::::::::::::::::::::::::::::::::::::',
                //     '\n::this::', this,
                //     '\n::$event::', $event,
                //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                // );
                this.onDragStart($event);
            },
            onDragEndScope: this,
            onDragEnd: ($event) => {
                // console.log(
                //     '\n:::::::::::::::::::::::::::::::::::::: DraggableService makeDraggable onDragEnd :::::::::::::::::::::::::::::::::::::::::::::::::::',
                //     '\n::this::', this,
                //     '\n::$event::', $event,
                //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                // );
                this.onDragEnd($event);
            },
        });

    }
}
