import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsStatesService {
  private dataSubject: BehaviorSubject<object[]> = new BehaviorSubject([]);
  public data$: Observable<object[]> = this.dataSubject.asObservable();

  /**
   * Creates a new API Get Service via environment specified local.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(/*X*/
              public http: HttpClient
              /*X*/) {
  }

  public getDatas(): any {
    console.log(
        '\n::::::::::::::::::::::::::::::::::::::  UsStatesService  getDatas  :::::::::::::::::::::::::::::::::::::::::::::::::::',
        '\n::this::', this,
        '\n::environment.localData + \'geo-us.json\'::', environment.localData + 'us-states.json',
        '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
    );
    return this.http.get(environment.localData + 'us-states.json', {responseType: 'json'});
  }

}

