import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GetElementAngleService {

    constructor() {
    }

    /*
        private getAngle(startX, startY, startX, startY): number {

            return this.calculateAngle();
        }

        private calculateAngle(): number {
            /!* determine angle of longest line segement relative to x axis *!/
            let dY: number = this.link.source['y'] - this.link.target['y'];
            let dX: number = this.link.source['x'] - this.link.target['x'];
            let angleInDegrees: number = (Math.atan2(dY, dX) / Math.PI * 180.0);

            // let angle = Math.PI + Math.atan2(-event.x + base.x, event.y - base.y);
            // let angle = Math.PI + Math.atan2(-event.x + base.x, event.y - base.y);
            let angle = Math.PI + Math.atan2(-this.link.target['y'] + this.link.source['y'], this.link.target['x'] - this.link.source['x']);
            // console.log(angle * 180 / Math.PI);
            /!* if text would be upside down, rotate through 180 degrees: *!/
            if ( (angleInDegrees > 90 && angleInDegrees < 270) || (angleInDegrees < -90 && angleInDegrees > -270) ) {
                angleInDegrees += 180;
                angleInDegrees %= 360;
            }
            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: GetElementAngleService getAngle :::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::this::', this,
                '\n::dX::', dX,
                '\n::dY::', dY,
                '\n::angleInDegrees::', angleInDegrees,
                '\n::angle * 180 / Math.PI::', angle * 180 / Math.PI,
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );

            return angleInDegrees;
        }
    */

    /*    private atan(v1x, v1y, v2x, v2y): any {
            let theta;

            // let v1 = {x: v1x, y: v1y};
            // let v2 = {x: v2x, y: v2y};
            let angleRad = Math.acos((v1x * v2x + v1y * v2y) / (Math.sqrt(v1x * v1x + v1y * v1y) * Math.sqrt(v2x * v2x + v2y * v2y)));
            let angleDeg = angleRad * 180 / Math.PI;

            let angle = Math.atan2(v2y, v2x) - Math.atan2(v1y, v1x);

            let slope = Math.atan2((v2y - v1y), (v2x - v1x));
            // if (angle < 0) { angle += 2 * Math.PI; }
            /!*     if ( angle > Math.PI ) {
                     angle -= 2 * Math.PI;
                 } else if ( angle <= -Math.PI ) {
                     angle += 2 * Math.PI;
                 }*!/

            if ( slope < 0 ) {
                theta = 180 + slope; // range [0, 360)
            } else {
                theta = slope;
            }

            if ( slope / Math.PI * 180.0 > 180 && slope / Math.PI * 180.0 < 360 ) {
                console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: GraphLinkPathComponent angle true :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::theta::', theta,
                    '\n::slope / Math.PI * 180.0::', slope / Math.PI * 180.0,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            } else {
                console.log(
                    '\n:::::::::::::::::::::::::::::::::::::: GraphLinkPathComponent angle false :::::::::::::::::::::::::::::::::::::::::::::::::::',
                    '\n::this::', this,
                    '\n::theta::', theta,
                    '\n::slope / Math.PI * 180.0::', slope / Math.PI * 180.0,
                    '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                );
            }
            /!*        console.log(
                        '\n:::::::::::::::::::::::::::::::::::::: GraphLinkPathComponent angle :::::::::::::::::::::::::::::::::::::::::::::::::::',
                        '\n::this::', this,
                        '\n::slope::', slope,
                        '\n::slope / Math.PI * 180.0::', slope / Math.PI * 180.0,
                        '\n::angle::', angle,
                        '\n::angleRad::', angleRad,
                        '\n::angleDeg::', angleDeg,
                        '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
                    );*!/

            // return theta;
        }*/

    /*
        private angle(cx, cy, ex, ey): number {
            let dy = ey - cy;
            let dx = ex - cx;
            let theta = Math.atan2(dy, dx); // range (-PI, PI]
            theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
            if ( theta < 0 ) {
                theta = 360 + theta; // range [0, 360)
            }
            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: GraphLinkPathComponent angle :::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::this::', this,
                '\n::theta::', theta,
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );

            return theta;
        }
    */

    /*  private getAngle(): number {
        /!* determine angle of longest line segement relative to x axis *!/
        let dY: number = this.link.source['y'] - this.link.target['y'];
        let dX: number = this.link.source['x'] - this.link.target['x'];
        let angleInDegrees: number = (Math.atan2(dY, dX) / Math.PI * 180.0);
        let angleDegree: boolean;

        // let angle = Math.PI + Math.atan2(-event.x + base.x, event.y - base.y);
        // let angle = Math.PI + Math.atan2(-event.x + base.x, event.y - base.y);
        // let angle = Math.PI + Math.atan2(-this.link.target['y'] + this.link.source['y'], this.link.target['x'] - this.link.source['x']);
        let angle = Math.PI + Math.atan2(this.link.source['y'] - this.link.target['y'], this.link.source['x'] - this.link.target['x']);
        // let  radians = Math.atan2(dY, dX);
        // if ( radians < 0 ) {
        //     radians += (2 * Math.PI);
        // }
        // let angle = Math.round( radians * (180/Math.PI));
        // let angle = Math.PI + Math.atan2(this.link.target['x'] - this.link.source['x'], -this.link.target['y'] + this.link.source['y'],);
        // console.log(angle * 180 / Math.PI);
        /!* if text would be upside down, rotate through 180 degrees: *!/
        if ( (angle > 90 && angle < 270) || (angle < -90 && angle > -270) ) {
          // if ( (angleInDegrees > 90 && angleInDegrees < 270) || (angleInDegrees < -90 && angleInDegrees > -270) ) {
          angleInDegrees += 180;
          angleInDegrees %= 360;
          angleDegree = true;
        } else {
          angleDegree = false;
        }
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: GraphLinkPathComponent getAngle :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::dX::', dX,
            '\n::dY::', dY,
            '\n::angleDegree::', angleDegree,
            '\n::angle::', angle,
            // '\n::angleInDegrees::', angleInDegrees,
            '\n::angle * 180 / Math.PI::', angle * 180 / Math.PI,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );

        return angleInDegrees;
      }*/

    /*
        getAngle(d, i) {
            //Search pattern for everything between the start and the first capital L
            var firstArcSection = /(^.+?)L/;

            //Grab everything up to the first Line statement
            var newArc = firstArcSection.exec(d3.select(this).attr("d"))[1];
            //Replace all the commas so that IE can handle it
            newArc = newArc.replace(/,/g, " ");

            //If the end angle lies beyond a quarter of a circle (90 degrees or pi/2)
            //flip the end and start position
            if ( d.endAngle > 90 * Math.PI / 180 ) {
                //Everything between the capital M and first capital A
                var startLoc = /M(.*?)A/;
                //Everything between the capital A and 0 0 1
                var middleLoc = /A(.*?)0 0 1/;
                //Everything between the 0 0 1 and the end of the string (denoted by $)
                var endLoc = /0 0 1 (.*?)$/;
                //Flip the direction of the arc by switching the start and end point
                //and using a 0 (instead of 1) sweep flag
                var newStart = endLoc.exec(newArc)[1];
                var newEnd = startLoc.exec(newArc)[1];
                var middleSec = middleLoc.exec(newArc)[1];

                //Build up the new arc notation, set the sweep-flag to 0
                newArc = "M" + newStart + "A" + middleSec + "0 0 0 " + newEnd;
            }
        }
    */
}
