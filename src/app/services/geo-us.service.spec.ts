import { TestBed } from '@angular/core/testing';

import { GeoUsService } from './geo-us.service';

describe('GeoUsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeoUsService = TestBed.get(GeoUsService);
    expect(service).toBeTruthy();
  });
});
