import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FlattenDataService {

    constructor() {
    }

    private flatten(array) {
        let result = [];
        array.forEach((nestedArray) => {
            result.push(nestedArray);
            if ( Array.isArray(nestedArray.children) ) {
                result = result.concat(this.flatten(nestedArray.children));
            }
        });
        return result;
    }

    private flatter(data) {
        return data.reduce((result, next) => {
            result.push(next);
            if ( next.children ) {
                result = result.concat(this.flatter(next.children));
                // next.children = [];
            }
            return result;
        }, []);
    }

    public getFlatted(data: Array<any>): Array<any> {
        // return this.flatten(data);
        return this.flatter(data);
    }

}
