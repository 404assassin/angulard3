import { TestBed } from '@angular/core/testing';

import { FlattenDataService } from './flatten-data.service';

describe('FlattenDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlattenDataService = TestBed.get(FlattenDataService);
    expect(service).toBeTruthy();
  });
});
