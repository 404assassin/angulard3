import { TestBed } from '@angular/core/testing';

import { BoundingBoxOffsetService } from './bounding-box-offset.service';

describe('BoundingBoxOffsetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BoundingBoxOffsetService = TestBed.get(BoundingBoxOffsetService);
    expect(service).toBeTruthy();
  });
});
