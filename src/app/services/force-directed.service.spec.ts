import { TestBed } from '@angular/core/testing';

import { ForceDirectedService } from './force-directed.service';

describe('ForceDirectedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ForceDirectedService = TestBed.get(ForceDirectedService);
    expect(service).toBeTruthy();
  });
});
