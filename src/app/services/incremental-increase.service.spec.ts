import { TestBed } from '@angular/core/testing';

import { IncrementalIncreaseService } from './incremental-increase.service';

describe('IncramentalRangedIncreaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncrementalIncreaseService = TestBed.get(IncrementalIncreaseService);
    expect(service).toBeTruthy();
  });
});
