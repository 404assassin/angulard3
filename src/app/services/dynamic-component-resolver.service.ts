import { Injectable, ComponentFactoryResolver, Type } from '@angular/core';
import { DynamicComponentType } from '../content/d3/views/shared/dynamic-component-loader/dynamic-component-type';
import {SystemMessageComponent} from '../content/d3/views/shared/system-message/system-message.component';

@Injectable({
    providedIn: 'root'
})
// class CustomType { componentType: Type<any>; component: Type<any>};
export class DynamicComponentResolverService {
    private components: any = [];

    constructor(/*MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM*/
                private componentFactoryResolver: ComponentFactoryResolver,
                /*MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM*/) {

    }

/*    public setDynamicComponents(displayType: object) {
        let factoryClass: any;
        const factories = Array.from(this.componentFactoryResolver['_factories'].values());
        if ( typeof <Type<any>>factories.find((x: any) => x.componentType.name === displayType['type']) !== 'undefined' ) {
            factoryClass = <Type<any>>factories.find((x: any) => x.componentType.name === displayType['type']);
            this.components.push(new DynamicComponentType(factoryClass.componentType, displayType['data']));
        }
        this.components.push(new DynamicComponentType(SystemMessageComponent, {name: 'Bombasto', bio: 'Brave as they come'}));
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: DynamicComponentResolverService setDynamicComponents :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::factories::', factories,
            '\n::factoryClass::', factoryClass,
            '\n::factoryClass.component::', factoryClass.component,
            '\n::factoryClass.componentType::', factoryClass.componentType,
            '\n::this.components::', this.components,
            '\n::this.componentFactoryResolver::', this.componentFactoryResolver,
            '\n::this.componentFactoryResolver[\'_factories\'::', this.componentFactoryResolver['_factories'],
            '\n::this.componentFactoryResolver[\'_factories\'].values()::', this.componentFactoryResolver['_factories'].values(),
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );

    }*/

    public getDynamicComponents(displayType: any) {
        let factoryClass: any;
        const factories = Array.from(this.componentFactoryResolver['_factories'].values());
        if ( typeof <Type<any>>factories.find((x: any) => x.componentType.name === displayType['type']) !== 'undefined' ) {
            factoryClass = <Type<any>>factories.find((x: any) => x.componentType.name === displayType['type']);
            this.components.push(new DynamicComponentType(factoryClass.componentType, displayType['data']));
/*            console.log(
                '\n:::::::::::::::::::::::::::::::::::::: DynamicComponentResolverService getDynamicComponents :::::::::::::::::::::::::::::::::::::::::::::::::::',
                '\n::this::', this,
                '\n::factories::', factories,
                '\n::factoryClass::', factoryClass,
                // '\n::factoryClass.component::', factoryClass.component,
                '\n::factoryClass.componentType::', factoryClass.componentType,
                '\n::this.components::', this.components,
                '\n::this.componentFactoryResolver::', this.componentFactoryResolver,
                '\n::this.componentFactoryResolver[\'_factories\'::', this.componentFactoryResolver['_factories'],
                '\n::this.componentFactoryResolver[\'_factories\'].values()::', this.componentFactoryResolver['_factories'].values(),
                '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
            );*/
            return this.components;
        }
    }
}
