import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class BoundingBoxOffsetService {
    /**
     * Takes an element reference as an argument and return an object with the bounding box and offset values
     **/
    public getBoundingBoxOffset(elem: any): any {
        let box: any = elem.getBoundingClientRect();
        let body: any = document.body;
        let docElem: any = document.documentElement;
        let scrollTop: number = window.pageYOffset || docElem.scrollTop || body.scrollTop;
        let scrollLeft: number = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
        let clientTop: number = docElem.clientTop || body.clientTop || 0;
        let clientLeft: number = docElem.clientLeft || body.clientLeft || 0;
        let top: number = box.top + scrollTop - clientTop;
        let left: number = box.left + scrollLeft - clientLeft;
        let bottom: number = top + (box.bottom - box.top);
        let right: number = left + (box.right - box.left);
        let x: number = box.x;
        let horizontalCenter: number = (box.right - box.left) / 2 + left;
        let verticalCenter: number = (box.bottom - box.top) / 2 + left;

        // console.info(
        //     '\n::::::::::::::::::::::::::::::::::  BoundingBoxOffsetService getBoundingBoxOffset  :::::::::::::::::::::::::::::::::::::',
        //     '\n::elem::', elem,
        //     '\n::box::', box,
        //     '\n::scrollTop::', scrollTop,
        //     '\n::horizontalCenter::', horizontalCenter,
        //     // '\n::typeof box::', typeof box,
        //     // '\n::body::', body,
        //     // '\n::typeof body::', typeof body,
        //     '\n::docElem::', docElem,
        //     // '\n::typeof docElem::', typeof docElem,
        //     '\n::scrollTop::', scrollTop,
        //     // '\n::typeof scrollTop::', typeof scrollTop,
        //     '\n::scrollLeft::', scrollLeft,
        //     '\n::x::', x,
        //     '\n::clientTop::', clientTop,
        //     // '\n::typeof clientTop::', typeof clientTop,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );

        return {
            scrollTop: Math.round(scrollTop),
            top: Math.round(top),
            left: Math.round(left),
            bottom: Math.round(bottom),
            right: Math.round(right),
            x: Math.round(x),
            horizontalCenter: Math.round(horizontalCenter),
            verticalCenter: Math.round(verticalCenter),
        };
    }
}
