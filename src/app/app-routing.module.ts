import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
// import {ContentModule} from './content/content.module';

const routes: Routes = [
    {path: '', redirectTo: 'types', pathMatch: 'full'},
    // {path: 'logout', component: LogoutComponent},
    // {path: 'login', component: LoginComponent},
    {path: 'types', component: ContentComponent},
    // {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes,
        {useHash: true})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
