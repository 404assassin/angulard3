import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[appContentHost]'
})
export class ContentHostDirective {

    constructor(/*[[][][][][][][][][][][][][][][][][]]*/
                public viewContainerRef: ViewContainerRef
                /*[[][][][][][][][][][][][][][][][][]]*/) {
        viewContainerRef.constructor.name === "ViewContainerRef_"; // true

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ContentHostDirective constructor :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::viewContainerRef.constructor.name === "ViewContainerRef_"::', viewContainerRef.constructor.name === "ViewContainerRef_",
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }

}
