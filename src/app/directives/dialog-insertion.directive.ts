import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[appDialogInsertion]'
})
export class DialogInsertionDirective {

    constructor(/*ōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōō*/
                public viewContainerRef: ViewContainerRef
                /*ōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōōō*/) {
    }

}
