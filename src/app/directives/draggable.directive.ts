import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { GraphForceDirected } from '../content/d3/models/graph-force-directed.model';
import { D3Service } from '../services/d3/d3.service';
import { GraphNode } from '../content/d3/models/graph-node.model';

@Directive({
    selector: '[appDraggable]'
})
export class DraggableDirective implements OnInit {

    @Input() draggableNode: GraphNode;
    @Input() draggableInGraph: GraphForceDirected;

    constructor(/*::::::::::||{/\}||::::::::::*/
                private d3Service: D3Service,
                private elementRef: ElementRef
                /*::::::::::||{/\}||::::::::::*/) {
    }

    ngOnInit() {
        this.d3Service.applyDraggableBehaviour(this.elementRef.nativeElement, this.draggableNode, this.draggableInGraph);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: DraggableDirective ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.draggableNode::', this.draggableNode,
        //     '\n::this.draggableInGraph::', this.draggableInGraph,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

}
