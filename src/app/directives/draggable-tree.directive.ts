import { Directive, ElementRef, Input } from '@angular/core';
import { GraphNode } from '../content/d3/models/graph-node.model';
import { TreeGraphForceDirected } from '../content/d3/models/tree-graph-force-directed.model';
import { D3Service } from '../services/d3/d3.service';

@Directive({
    selector: '[appDraggableTreeElement]'
})
export class DraggableTreeDirective {

    @Input() draggableNode: GraphNode;
    @Input() treeGraphForceDirected: TreeGraphForceDirected;

    constructor(/*::::::::::||{/\}||::::::::::*/
                private d3Service: D3Service,
                private elementRef: ElementRef
                /*::::::::::||{/\}||::::::::::*/) {
    }

    ngOnInit() {
        this.d3Service.applyTreeDraggableBehaviour(this.elementRef.nativeElement, this.draggableNode, this.treeGraphForceDirected);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: DraggableTreeDirective ngOnInit :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::this.draggableNode::', this.draggableNode,
        //     '\n::this.treeGraphForceDirected::', this.treeGraphForceDirected,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

}
