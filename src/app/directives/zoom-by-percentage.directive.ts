import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { GeoService } from '../services/d3/geo.service';
import { IncrementalIncreaseService } from '../services/incremental-increase.service';
import { Power2, TweenMax } from 'gsap/all';

// import { D3Service } from '../../../services/d3/d3.service';

@Directive({
    selector: '[appZoomByPercentage]'
})
export class ZoomByPercentageDirective {

    @Input() elementContainer: any;
    @Input() zoomableElement: ElementRef;
    @Input() zoomPercentage: any;
    @Input() zoomOffsetX: any;
    @Input() zoomOffsetY: any;
    @Input() activeElement: string;
    private zoomAmount: number;
    public zoomIconStates: any = {
        zoom: true,
        minus: false,
        reset: false
    };

    constructor(/*::::::::::||{/\}||::::::::::*/
                private geoService: GeoService,
                private elementRef: ElementRef,
                private incrementalIncreaseService: IncrementalIncreaseService,
                /*::::::::::||{/\}||::::::::::*/) {
    }

    @HostListener('click', ['$event.target']) onClick(clickTarget) {
        // this.zoomAmount = this.incrementalIncreaseService.incrementalIncrease(1, 10, 100);
        // this.activeElement = this.zoomableElement['elementRef'].nativeElement.id;
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ZoomByPercentageDirective onClick :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::clickTarget::', clickTarget,
            '\n::this.zoomIconStates::', this.zoomIconStates,
            '\n::this.zoomAmount::', this.zoomAmount,
            '\n::this.zoomableElement::', this.zoomableElement,
            '\n::this.zoomableElement.nativeElement::', this.zoomableElement.nativeElement,
            // '\n::this.zoomableElement[\'elementRef\'].nativeElement::', this.zoomableElement['elementRef'].nativeElement,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        // this.getButtonState(clickTarget);
        // this.geoService.zoomToBoundingBox(this.elementContainer, clickTarget, this.zoomableElement['elementRef'].nativeElement, this.geoPath, this.pathData, this.translateParams);
        this.geoService.zoomPercentage(this.elementContainer, this.zoomableElement, this.zoomPercentage, this.zoomOffsetX, this.zoomOffsetY)
    }

    private getButtonState(clickTarget: any) {
        if ( 'plus-icon' ) {
            this.zoomIconStates = {
                zoom: true,
                minus: false,
                reset: false
            };
        }
        if ( 'minus-icon' ) {
            this.zoomIconStates = {
                zoom: false,
                minus: true,
                reset: false
            };
        }

        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ZoomByPercentageDirective getButtonState :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::clickTarget::', clickTarget,
            '\n::clickTarget.id::', clickTarget.id,
            '\n::this.zoomIconStates::', this.zoomIconStates,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
        this.setButtonState(clickTarget)
    }

    private setButtonState(clickTarget: any) {

        this.zoomIconStates = {
            zoom: true,
            minus: true,
            reset: true
        };

        TweenMax.to(this.elementContainer.nativeElement.getElementById('plus-icon'), 0.75, {
            autoAlpha: 0.50,
            ease: Power2.easeInOut,
        });
        console.log(
            '\n:::::::::::::::::::::::::::::::::::::: ZoomByPercentageDirective setButtonState :::::::::::::::::::::::::::::::::::::::::::::::::::',
            '\n::this::', this,
            '\n::clickTarget::', clickTarget,
            '\n::clickTarget.id::', clickTarget.id,
            '\n::this.zoomIconStates::', this.zoomIconStates,
            '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        );
    }
}
