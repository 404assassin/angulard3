import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { D3Service } from '../services/d3/d3.service';

@Directive({
    selector: '[appZoomable]'
})
export class ZoomableDirective implements OnInit {

    @Input() zoomableOf: any;

    constructor(/*::::::::::||{/\}||::::::::::*/
                private d3Service: D3Service,
                private elementRef: ElementRef,
                /*::::::::::||{/\}||::::::::::*/) {
    }

    ngOnInit() {
        this.d3Service.applyZoomableBehaviour(this.zoomableOf, this.elementRef.nativeElement);
    }
}
