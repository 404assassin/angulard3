import { Directive, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { D3Service } from '../services/d3/d3.service';

import { GeoService } from '../services/d3/geo.service';

@Directive({
    selector: '[appZoomableToBounding]'
})
export class ZoomableToBoundingDirective implements OnInit {

    @Input() elementContainer: any;
    @Input() zoomableElement: ElementRef;
    @Input() geoPath: ElementRef;
    @Input() pathData: ElementRef;
    @Input() translateParams: ElementRef;
    @Input() activeElement: string;
    @Input() elementIndex: string;
    @Output() selectedState: EventEmitter<any> = new EventEmitter();

    constructor(/*::::::::::||{/\}||::::::::::*/
                private d3Service: D3Service,
                private geoService: GeoService,
                private elementRef: ElementRef,
                /*::::::::::||{/\}||::::::::::*/) {
    }

    ngOnInit() {
        // this.d3Service.zoomToBoundingBox(this.zoomableToBounding, this.elementRef.nativeElement);
        // this.geoService.zoomToBoundingBox(this.zoomableToBounding, this.elementRef.nativeElement);
        // zoomToBoundingBox(svgPathElement: any, path: any, pathData: any, translateParams: any): void {
    }

    @HostListener('click', ['$event.target']) onClick(clickTarget) {
        this.activeElement = this.zoomableElement['elementRef'].nativeElement.id;
        this.updateUseReference(this.activeElement);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ZoomableToBoundingDirective onClick :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::clickTarget::', clickTarget,
        //     '\n::this.zoomableElement[\'elementRef\'].nativeElement::', this.zoomableElement['elementRef'].nativeElement,
        //     '\n::this.geoPath::', this.geoPath,
        //     '\n::this.pathData::', this.pathData,
        //     '\n::this.translateParams::', this.translateParams,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.geoService.zoomToBoundingBox(this.elementContainer, clickTarget, this.zoomableElement['elementRef'].nativeElement, this.geoPath, this.pathData, this.translateParams);
    }

    private updateUseReference(activeElementId: any): void {
        this.selectedState.emit(this.elementIndex);
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: ZoomableToBoundingDirective updateUseReference :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::activeElementId::', activeElementId,
        //     '\n::this.elementContainer::', this.elementContainer,
        //     '\n::this.elementRef.nativeElement::', this.elementRef.nativeElement,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }

}
