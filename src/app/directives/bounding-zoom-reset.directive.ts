import { Directive, ElementRef, EventEmitter, HostListener, Input, Output, Renderer2 } from '@angular/core';

import { GeoService } from '../services/d3/geo.service';

// import { D3Service } from '../../../services/d3/d3.service';

@Directive({
    selector: '[appBoundingZoomReset]'
})
export class BoundingZoomResetDirective {

    @Input() elementContainer: any;
    @Input() zoomableElement: ElementRef;
    @Input() geoPath: ElementRef;
    @Input() pathData: ElementRef;
    @Input() translateParams: ElementRef;
    @Input() activeElement: string;
    @Output() resetEvent: EventEmitter<any> = new EventEmitter();

    constructor(/*::::::::::||{/\}||::::::::::*/
                private geoService: GeoService,
                /*::::::::::||{/\}||::::::::::*/) {
    }

    @HostListener('click', ['$event', '$event.target']) onClick($event, $eventTarget) {
        // this.activeElement = this.zoomableElement['elementRef'].nativeElement.id;
        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: BoundingZoomResetDirective onClick :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n::$event::', $event,
        //     '\n::$eventTarget::', $eventTarget,
        //     '\n::this.elementContainer::', this.elementContainer,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
        this.resetUpdate();
        this.geoService.resetBoundingBoxZoom($eventTarget, this.elementContainer);
    }

    private resetUpdate(): void {
        this.resetEvent.emit(null);

        // console.log(
        //     '\n:::::::::::::::::::::::::::::::::::::: BoundingZoomResetDirective updateUseReference :::::::::::::::::::::::::::::::::::::::::::::::::::',
        //     '\n::this::', this,
        //     '\n:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::'
        // );
    }
}
