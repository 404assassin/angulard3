import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { ContentModule } from './content/content.module';
import { DialogModule } from './content/d3/views/shared/dialog/dialog.module';

import { AppComponent } from './app.component';
import { DialogInsertionDirective } from './directives/dialog-insertion.directive';

@NgModule({
    declarations: [
        AppComponent,
        DialogInsertionDirective,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        ContentModule,
        DialogModule,
        HttpClientModule,
    ],
    providers: [],
    entryComponents: [],
    exports: [
    ],
    bootstrap: [
        AppComponent,
    ]
})
export class AppModule {
}
